
\begin{paragr}\label{paragr:defoograph}
  An \emph{$\oo$-graph} $C$ consists of a sequence $(C_k)_{k \in \mathbb{N}}$ of sets together with maps
  \[ \begin{tikzcd}
    C_{k-1} &\ar[l,"s",shift left] \ar[l,"t"',shift right] C_{k}
  \end{tikzcd}
  \]
  for every $k > 0$, subject to the \emph{globular identities}:
  \begin{equation*}
  \left\{
  \begin{aligned}
    s \circ s &= s \circ t, \\
    t \circ t &= t \circ s.
  \end{aligned}
  \right.
  \end{equation*}
  Elements of $C_k$ are called \emph{$k$-cells} or \emph{cells of dimension $k$}.
  For $x$ a $k$-cell with $k>0$, $s(x)$ is the \emph{source} of $x$ and $t(x)$ is the \emph{target} of $x$.
  
  More generally for all $k<n \in \mathbb{N}$, we define maps $s_k,t_k : C_n \to C_k$ as
  \[
  s_k = \underbrace{s\circ \dots \circ s}_{n-k \text{ times}}
  \]
  and
    \[
  t_k = \underbrace{t\circ \dots \circ t}_{n-k \text{ times}}.
  \]
  For an $n$-cell $x$, $s_k(x)$ is the \emph{$k$-source} of $x$ and $t_k(x)$ the \emph{$k$-target} of $x$.


  Two $k$-cells $x$ and $y$ are \emph{parallel} if $k=0$ or $k>0$ and
  \[
  s(x)=s(y) \text{ and } t(x)=t(y).
  \]
   For all $k<n \in \mathbb{N}$, we define the set $C_n\underset{C_k}{\times}C_n$ as the following fibred product
    \[
    \begin{tikzcd}
      C_n\underset{C_k}{\times}C_n \ar[r] \ar[dr,phantom,"\lrcorner", very near start] \ar[d] &C_n \ar[d,"t_k"]\\
      C_n \ar[r,"s_k"] & C_k.
      \end{tikzcd}
    \]
    That is, elements of $C_n\underset{C_k}{\times}C_n$ are pairs $(x,y)$ of $n$-cells such that $s_k(x)=t_k(y)$. We say that two $n$-cells $x$ and $y$ are \emph{$k$-composable} if the pair $(x,y)$ belongs to $C_n\times_{C_k}C_n$. 
\end{paragr}

\begin{paragr}\label{paragr:defmoroograph}
    Let $C$ and $C'$ be two $\oo$-graphs. A \emph{morphism of $\oo$-graphs} $f : C \to C'$ from $C$ to $C'$ is a sequence $(f_k : C_k \to D_k)_{k \in \mathbb{N}}$ of maps such that for every $k>0$, the squares
    \[
    \begin{tikzcd}
    C_k \ar[d,"s"] \ar[r,"f_k"]&C'_k \ar[d,"s"] \\
    C_{k-1} \ar[r,"f_{k-1}"] & C'_{k-1}
    \end{tikzcd}
\quad
        \begin{tikzcd}
    C_k \ar[d,"t"] \ar[r,"f_k"]&C'_k \ar[d,"t"] \\
    C_{k-1} \ar[r,"f_{k-1}"] & C'_{k-1}
    \end{tikzcd}
    \]
    are commutative.

    For a $k$-cell $x$, we will often write $f(x)$ instead of $f_k(x)$.
    We denote by $\ooGrph$ the category of $\oo$-graphs and morphisms of $\oo$-graphs.
\end{paragr}
\begin{paragr}
  For $n \in \mathbb{N}$, the notion of \emph{$n$-graph} is defined similarly, only this time there is only a finite sequence $(C_k)_{0 \leq k \leq n}$ of cells.
  For example, a $0$-graph is just a set and a $1$-graph is an ordinary graph
  \[
  \begin{tikzcd}
    C_0 & \ar[l,shift right,"t"'] \ar[l,shift left,"s"] C_1.
    \end{tikzcd}
  \]
  The definition of morphism of $n$-graphs is the same as for $\omega$-graphs, only this time there is only a finite sequence $(f_k : C_k \to C'_k)_{0 \leq k \leq n}$ of maps. We denote by $\nGrph$ the category of $n$-graphs and morphisms of $n$-graphs.  
\end{paragr}
\begin{paragr}
  Let $n \in \nbar$. An \emph{$n$-magma} consists of:
  \begin{itemize}
  \item[-] an $n$-graph $C$,
  \item[-] maps
    \[
    \begin{aligned}
    (\shortminus)\underset{k}{\ast}(\shortminus)  : C_l\underset{C_k}{\times}C_l &\to C_l \\
      (x,y) &\mapsto x\underset{k}{\ast}y
      \end{aligned}
    \]
    for all $l,k \in \mathbb{N}$ with $k < l \leq n$,\footnote{Note that if $n=\omega$, then $l<n$ because we supposed that $l \in \mathbb{N}$.}
  \item[-] maps
    \[
    \begin{aligned}
    1_{(\shortminus)} : C_k &\to C_{k+1}\\
      x &\mapsto 1_x
      \end{aligned}
    \]
    for every $k \in \mathbb{N}$ with $k\leq n$,
  \end{itemize}
  subject to the following axioms:
  \begin{itemize}
  \item[-] for all $k,l \in \mathbb{N}$ with $k<l\leq n$ and every $k$-composable $l$-cells $x$ and $y$,
    \[
    s(x\underset{k}{\ast} y) =
    \begin{cases}
      s(y) &\text{ when }k=l-1,\\
      s(x)\underset{k}{\ast} s(y) &\text{ otherwise,}
      \end{cases}
    \]
    and
        \[
    t(x\underset{k}{\ast} y) =
    \begin{cases}
      t(x) & \text{ when }k=l-1,\\
      t(x)\underset{k}{\ast} t(y) &\text{ otherwise.}
      \end{cases}
    \]
  \item[-]for every $k \in \mathbb{N}$ with $k\leq n$ and every $k$-cell,
    \[
    s(1_x)=t(1_x)=x.
    \]
  \end{itemize}
  We will use the same letter to denote an $n$-magma and its underlying $n$-graph.
  
   For two $k$-composable $l$-cells $x$ and $y$, we refer to $x\ast_ky$ as the \emph{$k$-composition} of $x$ and $y$.
   For a $k$-cell $x$, we refer to $1_{x}$ as the \emph{unit on $x$}.
   
      \remtt{Je n'aime pas trop les notations et définitions des unités itérées qui suivent.}
    More generally, for any $l \in \mathbb{N}$ with $k < l\leq n$, we define $\1^{l}_{(\shortminus)} : C_k \to C_l$ as
    \[
    \1^{l}_{(\shortminus)} := \underbrace{1_{(\shortminus)} \circ \dots \circ 1_{(\shortminus)}}_{l-k \text{ times }} : C_k \to C_l.
    \]
    Let $x$ be a $k$-cell, $\1^l_x$ is the \emph{$l$-dimensional unit on $x$}, and for consistency, we also set
    \[
    \1^{k}_x := x.
    \]
    A cell is \emph{degenerate} if it is a unit on a strictly lower dimensional cell. 
\end{paragr}
\begin{paragr}
  Let $n \in \nbar$ and let $C$ and $C'$ be $n$-magmas. A \emph{morphism of $n$-magmas} $f : C \to C'$ is a morphism of $n$-graphs that is compatible with compositions and units. This means:
  \begin{itemize}
  \item[-]for all $k,l \in \mathbb{N}$ with $k<l\leq n$ and every $k$-composable $l$-cells $x$ and $y$,
    \[
    f(x\underset{k}{\ast}y)=f(x)\underset{k}{\ast}f(y),
    \]
  \item[-]for every $k \in \mathbb{N}$ with $k\leq n$ and every $k$-cell $x$,
    \[
    f(1_x)=1_{f(x)}.
    \]
  \end{itemize}
  We denote by $\nMag$ the category of $n$-magmas and morphisms of $n$-magmas.
\end{paragr}
\begin{paragr}
  Let $n \in \nbar$. An \emph{$n$-category} $C$ is an $n$-magma such that the following axioms are satisfied:
  \begin{enumerate}
    \item for all $k,l \in \mathbb{N}$ with $k<l\leq n$, for all $k$-composable $l$-cells $x$ and $y$, we have 
    \[
    1_{x\underset{k}{\ast}y}=1_{x}\underset{k}{\ast}1_{y},
    \]
  \item for all $k,l \in \mathbb{N}$ with $k<l\leq n$, for all $l$-cells $x, y$ and $z$ such that $x$ and $y$ are $k$-composable, and $y$ and $z$ are $k$-composable, we have
    \[
    (x\comp_{k}y)\comp_{k}z=x\comp_k(y\comp_kz),
    \]
    \item for all $k, l \in \mathbb{N}$, for all $n$-cells $x,x',y$ and $y'$  such that
    \begin{itemize}
    \item[-] $x$ and $y$ are $l$-composable, $x'$ and $y'$ are $l$-composable,
    \item[-] $x$ and $x'$ are $k$-composable, $y$ and $y'$ are $k$-composable,
    \end{itemize}
    we have
    \[
    ((x \ast^n_k x')\ast^n_l (y \ast^n_k y'))=((x \ast^n_l y)\ast^n_k (x' \ast^n_l y')).
    \]
    \end{enumerate}
  \end{paragr}
  \[
  \begin{tikzcd}[column sep=huge]
    n\Cat \ar[r,"\tau",""{name=B,above},""{name=C,below}] & (n\shortminus 1)\Cat \ar[l,bend right,"\iota"',""{name=A, below}] \ar[l,bend left,"\kappa",""{name=D, above}] \ar[from=A,to=B,symbol=\dashv]\ar[from=C,to=D,symbol=\dashv].
    \end{tikzcd}
  \]
   For every $k\in \mathbb{N}$ such that $k < n$, we define the set $\Sigma^{+}\underset{C_k}{\times}\Sigma^{+}$ as the following fibred product
    \[
    \begin{tikzcd}
      \Sigma^{+}\underset{C_k}{\times}\Sigma^{+} \ar[r] \ar[dr,phantom,"\lrcorner", very near start] \ar[d] &\Sigma^{+} \ar[d,"t_k"]\\
      \Sigma^{+} \ar[r,"s_k"] & C_k.
      \end{tikzcd}
    \]
    That is, elements of $\Sigma^{+}\underset{C_k}{\times}\Sigma^{+}$ are pairs $(x,y)$ of well formed words such that $s_k(x)=t_k(y)$. We say that two well formed words $x$ and $y$ are \emph{$k$-composable} if the pair $(x,y)$ belongs to $\Sigma^{+}\times_{C_k}\Sigma^{+}$. 

\end{paragr}
We define $s_k , t_k: \Sigma^{+} \to C_k$ as iterated source and target (with $s_n=s$ and $t_n=t$ for consistency). We say that two well formed words $v$ and $w$ are \emph{parallel} if
\[s(v)=s(w) \text{ and }t(v)=t(w).\]
and we say that they are \emph{$k$-composable} for a $k\leq n$ if
\[s_k(v)=t_k(w).\]
 It is straightforward to check that
\[
\begin{tikzcd}
  C_0 & \ar[l,shift right,"t"'] \ar[l,shift left,"s"] \cdots & \ar[l,shift right,"t"'] \ar[l,shift left,"s"] C_{n \shortminus 1} & \ar[l,shift right,"t"'] \ar[l,shift left,"s"] \Sigma^{+}
  \end{tikzcd}
\]
is an $n$-graph and
\section{Generating cells}
\begin{paragr}
  Let $n>0$, we define the category $n\CellExt$ of \emph{$n$-cellular extensions} as the following fibred product
  \begin{equation}\label{squarecellext}
    \begin{tikzcd}
  n\CellExt \ar[d] \ar[r] \ar[dr,phantom,"\lrcorner", very near start]&  (n \shortminus 1)\Cat \ar[d] \\
n\Grph  \ar[r,"\tau"] & (n \shortminus 1)\Grph,
  \end{tikzcd}
    \end{equation}
    where the right vertical arrow is the obvious forgetful functor.

    More concretely, an $n$-cellular extension can be encoded in the data of a quadruple $(\Sigma,C,s,t)$ where $\Sigma$ is a set, $C$ is a $(n\shortminus 1)$-category, $s$ and $t$ are maps
    \[
    s,t : \Sigma \to C_n
    \]
    such that
    \[
    \begin{tikzcd}
      C_{n-1} & \ar[l,shift right, "s"'] \ar[l,shift left,"t"] C_n & \ar[l,shift right, "s"'] \ar[l,shift left,"t"] \Sigma
      \end{tikzcd}
    \]
    satisfy the globular identities.

    Intuitively, a $n$-cellular extension is a $(n\shortminus 1)$-category with extra $n$-cells that make it a $n$-graph.  

    We will sometimes write
    \[
    \begin{tikzcd}
     C &\ar[l,shift right,"s"'] \ar[l,shift left,"t"]  \Sigma
      \end{tikzcd}
    \]
    to denote an $n$-cellular extension $(\Sigma,C,s,t)$. \remtt{Est-ce que je garde cette notation ?}
    
    A morphism of $n$-cellular extensions from  $(\Sigma,C,s,t)$ to $(\Sigma',C',s',t')$ consists of a pair $(\varphi,f)$ where $f : C \to C'$ is a morphism of $(n\shortminus 1)\Cat$ and $\varphi : \Sigma \to \Sigma'$ is a map such that the squares
    \[
    \begin{tikzcd}
      \Sigma \ar[r,"\varphi"] \ar[d,"s"] & \Sigma' \ar[d,"s'"] \\
      C_{n-1} \ar[r,"f_{n\shortminus 1}"] & C'_{n-1}
    \end{tikzcd}
    \text{ and }
        \begin{tikzcd}
      \Sigma \ar[r,"\varphi"] \ar[d,"t"] & \Sigma' \ar[d,"t'"] \\
      C_{n-1} \ar[r,"f_{n\shortminus 1}"] & C'_{n-1}
    \end{tikzcd}
        \]
        commute.
        
         Once again, we will use the notation $\tau$ for the functor
         \[
         \begin{aligned}
           \tau : n\CellExt &\to (n\shortminus 1)\Cat\\
           (\Sigma,C,s,t) &\mapsto C
           \end{aligned}
    \]
    which is simply the top horizontal arrow of square \eqref{squarecellext}. 
  \end{paragr}
\begin{paragr}
  Let $n>0$, we define the category $n\PCat$ of \emph{$n$-precategories} as the following fibred product
  \begin{equation}\label{squareprecat}
  \begin{tikzcd}
  n\PCat \ar[d] \ar[r] \ar[dr,phantom,"\lrcorner", very near start]& (n \shortminus 1)\Cat \ar[d] \\
  n\Mag \ar[r,"\tau"] & (n \shortminus 1)\Mag.
  \end{tikzcd}
  \end{equation}
  More concretely, objects of $n\PCat$ can be seen as $n$-magmas such that if we forget their $n$-cells then they satisfy the axioms of $(n\shortminus 1)$-category. The left vertical arrow of the previous square is easily seen to be full, and we will now consider that the category $n\PCat$ is a full subcategory of $n\Mag$. The top horizontal arrow of square \eqref{squareprecat} is simply the functor that forgets the $n$-cells. Once again, we will use the notation 
  \[
  \tau : n\PCat \to (n\shortminus 1)\Cat. 
  \]

  
  The commutative square
       \[
     \begin{tikzcd}
       n\Cat\ar[r,"\tau"] \ar[d] &(n \shortminus 1)\Cat\ar[d]\\
       n\Mag   \ar[r,"\tau"] & (n \shortminus 1)\Mag,
       \end{tikzcd}
     \]
     where the vertical arrows are the obvious forgetful functors, induces a canonical functor
     \[
     V : n\Cat \to n\PCat,
     \]
     which is also easily seen to be full. %and we will consider that $n\Cat$ is a full subcategory of $n\PCat$.

     Moreover, the canonical commutative diagram
     \[
     \begin{tikzcd}
       n\Mag \ar[d] \ar[r] & (n\shortminus 1)\Mag \ar[d] & (n\shortminus 1)\Cat \ar[d] \ar[l]\\
       n\Grph \ar[r] & (n\shortminus 1)\Grph & (n\shortminus 1)\Cat \ar[l]
     \end{tikzcd}
     \]
     induces a canonical functor
     \[
     W : n\PCat \to n\CellExt.
     \]
     For an $n$-precategory $C$, $W(C)$ is simply the cellular extension
     \[
         \begin{tikzcd}
     \tau(C) &\ar[l,shift right,"s"'] \ar[l,shift left,"t"]  C_n.
      \end{tikzcd}
     \]

     Finally, we define the functor
     \[
     U := W \circ V : n\Cat \to n\CellExt.
     \]
     The relation between $n\Cat$, $n\PCat$, $n\CellExt$ and $(n\shortminus 1)\Cat$ is summed up in the following commutative diagram:
     \[
     \begin{tikzcd}
       n\Cat \ar[rr,bend left,"U"]\ar[r,"V"] \ar[rrd,"\tau"'] & n\PCat \ar[r,"W"]\ar[rd,"\tau"] & n\CellExt \ar[d,"\tau"] \\
       &&(n\shortminus 1)\Cat.
       \end{tikzcd}
     \]
     We will now explicitely construct a left adjoint of $U$. In order to do that, we will successively construct left adjoints of $W$ and $V$.
\end{paragr}
\begin{paragr}
Let $E=(\Sigma,C,s,t)$ be an $n$-cellular extension. We define an $n$-precategory $W_!(E)$ with
\begin{itemize}
\item[-] $\tau(W_!(E))=C$,
\item[-] $W_!(E)_n=\Sigma^{+}$,
\item[-] source and target maps $\Sigma^+ \to C_{n-1}$ as defined in the previous paragraph,
\item[-] for every $x \in C_{n-1}$,
  \[1_x := (\ii_x)\]
\item[-] for every $v,w \in \Sigma^+$ that are $k$-composable for a $k<n$,
  \[
  v\comp_kw := (v \fcomp_k w).
  \]
\end{itemize}
It is straightforward to check that this defines an $n$-precategory. Let $(\varphi,f) : E \to E'$ be a morphism of $n$-cellular extensions. We define a morphism of $n$-precategories $W_!(\varphi,f) : W_!(E) \to W_!(E')$ with
\end{paragr}
 For $u : A \to B$ in $\CCat$, let
  \[
  u^* : \C(A) \to \C(B)
  \]
  be the functor induced by post-composition. For $\begin{tikzcd}        \sD(B)\ar[r,bend left,"u^*",""{name=U,below}] \ar[r,bend right,"v^*"',""{name=D,above}] & \sD(A) \ar[from=U,to=D,Rightarrow,"\alpha^*"] \end{tikzcd}$
  
 Note that we have a canonical isomorphism
  \[
  \C(e) \simeq \C
  \]
  and for any small category $A$, the functor
  \[
  p_A^* : \C(e) \to \C(A)
  \]
  is canonically isomorphic with the diagonal functor $\Delta : \C \to \C(A)$ that sends an object $X$ of $\C$ to the constant diagram $A \to \C$ with value $X$.
\begin{paragr}
  Let $(\C,\W)$ be a localizer and $A$ a small category. We denote by $\C^A$ the category of functors from $A$ to $\C$ and natural transformations between them. An arrow $\alpha : d \to d'$ of $\C^A$ is a \emph{pointwise weak equivalences} when $\alpha_a : d(a) \to d'(a)$ belongs to $\W$ for every $a \in A$. We denote by $\W_A$ the class of pointwise weak equivalences. This defines a localizer $(\C^A,\W_A)$.

  For every $u : A \to B$ morphism of $\Cat$, the functor induced by pre-composition
  \[
  u^* : \C^B \to \C^A
  \]
  preserves pointwise weak equivalences. Hence, there is an induced functor between the localized categories still denoted by $u^*$:
  \[
  u^* : \Ho(\C^B) \to \Ho(\C^A).
  \]
\end{paragr}
  Dually, if we are given a square in $\CCat$ of the form
   \[
     \begin{tikzcd}
    A \ar[r,"f"] \ar[d,"u"'] & B \ar[d,"v"]\\
    C \ar[r,"g"'] & D \ar[from=2-1,to=1-2,Rightarrow,"\alpha"]
  \end{tikzcd}
     \]
     and if $\sD$ has right Kan extension, then we obtain the cohomological base change morphism induced by $\alpha$:
     \[
     
     \]
  \end{paragr}


%%% Définition transfo pseudo-naturelle

   \iffalse
    such that the following axioms are satisfied:
    \begin{itemize}[label=-]
    \item for every small category $A$,
      \[
      F_{1_A}=1_{F(A)},
      \]
    \item for every pair of composable arrows $A \overset{u}{\rightarrow} B \overset{v}{\rightarrow} C$ in $\CCat$,
      \[
      \begin{tikzcd}
        \sD(C) \ar[d,"v^*"'] \ar[r,"F_C"] & \sD'(C) \ar[d,"v^*"]\\
        \ar[from=1-2,to=2-1,Rightarrow,"F_v","\sim"']
        \sD(B) \ar[d,"u^*"'] \ar[r,"F_B"] & \sD'(B) \ar[d,"u^*"]\\
        \sD(A) \ar[r,"F_A"'] & \sD'(A)
        \ar[from=2-2,to=3-1,Rightarrow,"F_u","\sim"']
      \end{tikzcd}
      =
      \begin{tikzcd}
        \sD(C) \ar[dd,"(vu)^*"'] \ar[r,"F_C"] & \sD'(C) \ar[dd,"(vu)^*"]\\
        &\\
        \sD(A) \ar[r,"F_A"'] & \sD'(A),
        \ar[from=1-2,to=3-1,Rightarrow,"F_{vu}","\sim"']
        \end{tikzcd}
      \]
    \item for every $\begin{tikzcd} A \ar[r,bend left,"u",""{name=A,below}] \ar[r,bend right,"v"',""{name=B,above}] & B  \ar[from=A,to=B,Rightarrow,"\alpha"] \end{tikzcd}$ in $\CCat$,
      \[
      \begin{tikzcd}
        \sD(B) \ar[d,"u^*",""{name=A,below}] \ar[r,"F_B"] & \sD'(B) \ar[d,"u^*"]\\
        \sD(A) \ar[r,"F_A"'] & \sD'(A)
        \ar[from=1-2,to=2-1,Rightarrow,"F_u","\sim"']
        \ar[from=1-1,to=2-1,bend right=55,"v^*"',""{name=B,below}]
        \ar[from=A,to=B,Rightarrow,"\alpha^*"',near start]
      \end{tikzcd}
      =
            \begin{tikzcd}
        \sD(B) \ar[d,"v^*"] \ar[r,"F_B"] & \sD'(B) \ar[d,"v^*"',""{name=D,below}]\\
        \sD(A) \ar[r,"F_A"'] & \sD'(A).
        \ar[from=1-2,to=2-1,Rightarrow,"F_v","\sim"']
        \ar[from=1-2,to=2-2,bend left=55,"u^*",""{name=C,above}]
        \ar[from=C,to=D,Rightarrow,"\alpha^*"',near start]
        \end{tikzcd}
      \]
    \end{itemize}
    \remtt{Ai-je vraiment besoin de donner la définition de transfo pseudo-naturelle ? }\fi

    %%% Localization derivator

    It is straightforward to check that we have the following universal property: for any op-prederivator $\sD$, the functor induced by pre-composition
    \[
    \gamma^* : \underline{\Hom}(\sD_{(\C,\W)},\sD) \to \underline{\Hom}(\C,\sD)
    \]
    is fully faithful and its essential image consists of morphisms of op-prederivators $F : \C \to \sD$ such that for every small category $A$, $F_A : \C(A) \to \sD(A)$ sends morphisms of $\W_A$ to isomorphisms of $\sD(A)$.

    This universal property is a higher version of the universal property of localization seen in \ref{paragr:loc}. It follows that given two localizers $(\C,\W)$ and $(\C',\W')$ and a functor $F : \C \to \C'$, if $F$ preserves weak equivalences, then there exists a morphism of op-prederivators $ \overline{F} : \sD_{(\C,\W)} \to \sD_{(\C',\W')}$ such that the square 
    \[
    \begin{tikzcd}
    \C \ar[r,"F"] \ar[d,"\gamma"] & \C' \ar[d,"\gamma'"]\\
    \sD_{(\C,\W)} \ar[r,"\overline{F}"] & \sD_{(\C',\W')}
  \end{tikzcd}
    \]
    is commutative.

    %%% Left derived functor derivator

      \begin{definition}
    Let $(\C,\W)$ and $(\C',\W')$ be two localizers. A functor $F : \C \to \C'$ is \emph{left derivable in the sense of derivators} if there exists a morphism $\LL F : \sD_{(\C,\W)} \to \sD_{(\C',\W')}$ and a $2$-morphism
  \[
  \begin{tikzcd}
    \C \ar[r,"F"] \ar[d,"\gamma"] & \C' \ar[d,"\gamma'"]\\
    \sD_{(\C,\W)} \ar[r,"\LL F"'] & \sD_{(\C',\W')}
    \arrow[from=2-1, to=1-2,"\alpha",Rightarrow]
  \end{tikzcd}
  \]
    that makes $\LL F$ the \emph{right} Kan extension of $\gamma' \circ F$ along $\gamma$ in the $2$-category of op-prederivators:
    \end{definition}

      %% Old version of representable op-prederivator

      \iffalse
\begin{example}\label{ex:repder}
  Let $\C$ be a category. For $A$ a small category, let $\C(A)$ be the category of functors $A \to \C$ and natural transformations between them.
  This canonically defines an op-prederivator
  \begin{align*}
    \C : \CCat^{op} &\to \CCAT \\
    A &\mapsto \C(A)
  \end{align*}
  where for any $u : A \to B$ in $\CCat$, the functor
  \[
  u^* : \C(A) \to \C(B)
  \]
  is induced from $u$ by pre-composition, and similarly for $\begin{tikzcd}A \ar[r,bend left,"u",""{name=A,below}] \ar[r,bend right, "v"',""{name=B,above}] & B \ar[from=A,to=B,Rightarrow,"\alpha"]\end{tikzcd}$ in $\CCat$, the natural transformation
  \[
  \begin{tikzcd}
    \C(B) \ar[r,bend left,"u^*",""{name=A,below}] \ar[r,bend right, "v^*"',""{name=B,above}] & \C(A) \ar[from=A,to=B,Rightarrow,"\alpha^*"]
    \end{tikzcd}
  \]
  is induced by pre-composition. This op-prederivator is sometimes referred to as the op-prederivator \emph{represented} by $\C$. Notice that for the terminal category $e$, we have a canonical isomorphism
  \[
  \C(e) \simeq \C
  \]
  that we shall use without further reference.
\end{example}\fi

%%% Garbage on op-prederivators

\iffalse When this is the case, we also use the notation
\[
  \hocolim_A : \sD_{(\C,\W)}(A) \to \sD_{(\C,\W)}(e)\simeq \ho(\C)
    \]
    instead of $p_{A!}$ for the left adjoint of $p_A^* : \sD_{(\C,\W)}(e) \to \sD_{(\C,\W)}(A)$ for any small category $A$. For a functor $d : A \to \C$,
    \[
    \hocolim_A(d)
    \]
    is the \emph{homotopy colimit of $d$} and for consistency, we also use the notation
    \[
    \hocolim_{a \in A}d(a).
    \]\fi
%
\iffalse\begin{paragr}
  An op-prederivator $\sD$ has \emph{right Kan extensions} when for every $u : A \to B$ in $\CCat$, the functor $u^* : \sD(B) \to \sD(A)$ has a right adjoint
  \[
  u_* : \sD(A) \to \sD(B).
  \]
  When $B$ is the terminal category $e$ and $u$ is the canonical functor $p_A : A \to e$, we use the notation
  \[
  \holim_A : \sD(A) \to \sD(e)
  \]
  instead of $p_{A*}$. For an object $X$ of $\sD(A)$,
  \[\holim_A(X)\]
  is the \emph{homotopy limit of $X$}.

  We leave it to the reader to dualize all the definitions, examples and assertions from \ref{ex:cocompletecat} to \ref{paragr:colimhocolim}. The reason we put emphasis on left Kan extensions is that homotopy colimits will play a central role in the sequel whereas we shall almost never make use of homotopy limits.
\end{paragr}
Asking for an op-prederivator to have left Kan extensions is one of the axioms in the definition of \emph{op-derivator}. Even though this axiom is the only one we will need in practise, all the op-prederivators we shall work with are actually op-derivators. Thus, for the sake of completeness, we shall now proceed to give the full definition of op-derivator.
\fi
  \iffalse \begin{paragr}
    Dually, for an op-prederivator that has right Kan extensions and for a $2$-square in $\CCat$
       \[
     \begin{tikzcd}
    A \ar[r,"f"] \ar[d,"u"'] & B \ar[d,"v"]\\
    C \ar[r,"g"'] & D, \ar[from=2-1,to=1-2,Rightarrow,"\alpha"]
  \end{tikzcd}
     \]
     we obtain a cohomological base change morphism:
     \[
     g^*v_*\Rightarrow u_* g^*.
     \]
     In particular, for $u : A \to B$ and $b$ an object of $B$, the canonical 2-square
     \[
     \begin{tikzcd}
     b\backslash A \ar[r,"k"] \ar[d,"p"'] & A \ar[d,"u"] \\
     e \ar[r,"b"'] & B
     \ar[from=2-1,to=1-2,Rightarrow,"\phi"]
     \end{tikzcd}
     \]
     where $b\backslash A:=(A^{op}/b)^{op}$ and, $k$ and $\phi$ are obtained by duality, yields a homological base change morphism:
     \[
     b^*u_* \Rightarrow p_*k^*.
     \]
     More intuitively, for $X$ an object of $\sD(A)$, this morphisms reads
     \[
     u_*(X)_b \to \holim_{b\backslash A}(X\vert_{b\backslash A}).
     \]
  \end{paragr}
  \begin{definition}[Grothendieck]
    An \emph{op-derivator} is an op-prederivator $\sD$ such that the following axioms are satisfied:
    \begin{enumerate}%[label=\textbf{DER \arabic*)},leftmargin=2cm]
    \item[\textbf{DER 1)}]\label{der1} \begin{enumerate}[label=\alph*)]
    \item For all $A,B$ small categories, the canonical functor
      \[
      \sD(A\amalg B)\rightarrow \sD(A)\times\sD(B),
      \]
      induced by the inclusions $A \to A\amalg B$ and $B \to A \amalg B$, is an equivalence of categories.
      \item  The canonical functor $\sD(\emptyset)\rightarrow e$ is an equivalence of categories.
    \end{enumerate}
    \item\label{der2} For every small category $A$, the functor
      \[
      \sD(A) \rightarrow \prod_{a \in \Ob(A)}\sD(e)
      \]
      induced by the functors $a^* : \sD(A)\to \sD(e)$ for all $a \in \Ob(A)$ (seen as morphisms $a : e \to A$), is conservative.
    \end{enumerate}
    \begin{enumerate}[label=\textbf{DER \arabic*d)},leftmargin=2.25cm,start=3]
    \item $\sD$ has left Kan extensions.
    \item For every $u : A \to B$ in $\CCat$, $b$ object of $B$ and $X$ object of $\sD(A)$, the homological base change morphism
      \[
       \hocolim_{A/b}(X\vert_{A/b}) \rightarrow u_!(X)_b,
       \]
       induced by the 2-square
       \[
       \begin{tikzcd}
         A/b \ar[r,"k"] \ar[d,"p"']& A \ar[d,"u"] \\
         e \ar[r,"b"'] & B
         \ar[from=1-2,to=2-1,Rightarrow,"\phi"]
       \end{tikzcd}
       \]
       is an isomorphism.
    \end{enumerate}
    \begin{enumerate}[label=\textbf{DER \arabic*g)},leftmargin=2.25cm,start=3]
    \item $\sD$ has left Kan extensions.
    \item For every $u : A \to B$ in $\CCat$, $b$ object of $B$ and $X$ object of $\sD(A)$, the cohomological base change morphism
      \[
      u_*(X)_b\rightarrow \holim_{b\backslash A}(X\vert_{b\backslash A})
      \]
      induced by the 2-square
           \[
     \begin{tikzcd}
     b\backslash A \ar[r,"k"] \ar[d,"p"'] & A \ar[d,"u"] \\
     e \ar[r,"b"'] & B
     \ar[from=2-1,to=1-2,Rightarrow,"\phi"]
     \end{tikzcd}
     \]
     is an isomorphism.
      \end{enumerate}
  \end{definition}
  \fi


  %%%%% Introduction

  
  Let $C$ be a small category. Recall that we have a canonical isomorphim
  \[
  \colim_{c \in C}C/c \simeq C,
  \]
  where $C/c$ is the slice category over the object $c$ of $C$. Now the idea of the proof is to show that this colimit is in fact a homotopy colimit both in $\ho(\oo\Cat^{\Th})$ and in $\ho(\oo\Cat^{\Th})$. This means respectively that:
  \begin{enumerate}[label=(\alph*)]
  \item the canonical morphism of $\ho(\oo\Cat^{\Th})$
    \[
    \hocolim_{c \in C}^{\Th}C/c \to \colim_{c \in C}C/c
    \]
    is an isomorphism,
  \item the canonical morphism of $\ho(\oo\Cat^{\folk})$
    \[
    \hocolim_{c \in C}^{\folk}C/c \to \colim_{c \in C}C/c
    \]
    is an isomorphism.
  \end{enumerate}
  (The subscript of the homotopy colimits are there to make sure not to remind in which ``homotopy theory'' on $\oo\Cat$ the homotopy colimit is to be understood.)

  Case (a) follows somewhat easily from Thomason's homotopy colimit theorem \cite{thomason1979homotopy}. Case (b) on the other hand is a new result and first appears in \cite{guetta2020homology}. The idea of the proof is as follows: let $f : P \to C$ a polygraphic resolution, i.e.\ $f$ is folk trivial fibration and $P$ is a free $\oo$-category, and consider the diagram
  \begin{align*}
    C &\to \oo\Cat \\
    c &\mapsto P/c,
  \end{align*}
  where $P/c$ is the $\oo$-category defined as the following fibred product
  \[
  \begin{tikzcd}
    P/c \ar[r] \ar[d] & P \ar[d,"f"] \\
    C/c \ar[r] & C. \ar[from=1-1,to=2-2,phantom,very near start,"\lrcorner"]
    \end{tikzcd}
  \]
  Using the fact that $f$ is a trivial fibration and some 2-out-of-3 property, the problem can be reduced to showing that the canonical map
  \[
  \hocolim^{\folk}_{c \in C}P/c \to \colim_{c \in C}P/c
  \]
  is an isomorphism of $\ho(\oo\Cat^{\folk})$. Now, this follows from the ``miracle'' that the diagram $c \mapsto P/c$ is a cofibrant object of the projective model structure on the category $\underline{\Hom}(C,\oo\Cat)$ induced by the folk model structure on $\oo\Cat$. The proof of the miracle is rather technical and needed the introduction of what I named \emph{discrete Conduché $\oo$-functor} in reference to the existing notion of discrete Conduché functor (or fibration) between categories (see \cite{johnstone1999note} for example). The key point is the following result, to which \cite{guetta2020polygraphs} is dedicated:
  \begin{center}
    Let $f : C \to D$ be a discrete Conduché $\oo$-functor. If $D$ is free then so is $C$.
  \end{center}
  Let us now come back to the the proof that all categories are homologically coherent. The second key result is that categories with final objects are homologically coherent and the homology of such categories is (the homology of) $\mathbb{Z}$ seen as a chain complex concentrated in degree $0$. In fact, these categories belong to a larger class of homologically coherent $\oo$-categories, referred to as \emph{contractible $\oo$-categories} in this dissertation. Finally, a formal argument shows that both polygraphic homology and Street homology preserve homotopy colimits. Altoghether, we have
  \[
  \sH^{\pol}(C) \simeq \hocolim_{c \in C}\sH^{\pol}(C/c) \simeq \hocolim_{c \in C} \mathbb{Z} \simeq \hocolim_{c \in C}\sH^{\Th}(C/c) \simeq \sH^{\Th}(C),
  \]
  and a thorough analysis of naturality shows that the resulting isomorphim $\sH^{\pol}(C) \simeq \sH^{\Th}(C)$ is the canonical comparison map.
  
  \iffalse It follows from Thomason's homotopy colimit theorem \cite{thomason1979homotopy} that this colimit is also homotopic when $\Cat$ is equipped with Thomason weak equivalences, which means that the canonical morphism
  \[
  \hocolim_{c \in C}^{\Th}C/c \to \colim_{c \in C}C/c
  \]
  is an isomorphism in $\ho(\Cat^{\Th})$. A formal argument using Gagna's theorem shows that the canonical inclusion $\Cat \to \oo\Cat$ preserves homotopy colimits (when both categories are equipped with Thomason weak equivalences), and in particular the above morphism is also an isomorphim of $\ho(\oo\Cat^{\Th})$ when the colimit and homotopy colimit are understood in $\oo\Cat$. Then, an easy argument shows that Street homology preserves homotopy colimits and, combined with the fact that a category with terminal object has the homotopy type of a point \cite[Paragraph 1]{quillen1973higher}, we obtain that
  \[
  \sH^{\St}(C)\simeq \hocolim_{c \in C}\sH^{\St}(C/c) \simeq \hocolim_{c \in C}\mathbb{Z},
  \]
  where $\mathbb{Z}$ is seen as chain complex (up to quasi-isomorphism) concentrated in degree $0$. Note that this formula for the homology of the nerve of a small category at least goes back to Gabriel and Zisman \cite[Appendix II, Proposition 3.3]{gabriel1967calculus}.

  On the other hand, it can also be shown that the colimit
  \[
  \colim_{c \in C}C/c \simeq C
  \]
  is a homotopy colimit in $\ho(\oo\Cat^{\folk})$. Contrary to the ``Thomason'' case, this result is new and first appears in \cite{guetta2020homology}. We shall come back to its proof shortly, but for now let us see how it implies that every category is homologically coherent. First, categories with terminal object belong to a class of what we refer to as \emph{contractible $\oo$-categories} and which are homologically coherent. In particular, we have \[\sH^{\pol}(C/c) \simeq \sH^{\St}(C/c) \simeq \mathbb{Z}\] for every object $c$ of $C$. Then, again by a formal argument, it can be shown that polygraphic homology preserves homotopy colimits and thus we have
  \[
  \sH^{\pol}(C) \simeq \hocolim_{c \in C}\sH^{\pol}(C/c) \simeq \hocolim_{c \in C}\mathbb{Z} \simeq \sH^{\St}(C)
  \]
  (and a thorough analysis shows that the isomorphim is in fact the canonical comparison map).

  Now, let us go back to the fact that the colimit $\colim_{c \in C}C/c$ is a homotopy colimit in $\ho(\oo\Cat^{\folk})$. Contrary to the ``Thomason'' case, we cannot reason in $\Cat$ because the inclusion $\Cat \to \oo\Cat$ does not
  \fi
\iffalse This means that $P$ is homologically coherent. On the other hand, it is not generally true that a folk cofibrant object $P'$ is Thomason cofibrant. However, \emph{if} the natural transformation $\pi$ in the above $2$-square was an isomorphism, then a quick 2-out-3 reasoning would show that the canonical map
  \[
  \LL \lambda^{\Th}(P') \to \lambda(P')
  \]
  is an isomorphism.
  \fi
%This result might sound surprising to the reader familiar with the fact that strict $\oo$-groupoids, even with weak inverses, do \emph{not} model homotopy types (as explained, for example, in \cite{simpson1998homotopy}). However, Gagna's result
\iffalse This result is in fact a generalization of a well-known result concerning the homotopy theory of (small) categories. Indeed, the category of (small) categories $\Cat$ can be identified with the full subcategory of $\oo\Cat$ with only unit cells above dimension $1$ and the restriction of $N_{\omega}$ to $\Cat$ is nothing but the usual functor for categories
\[
N : \Cat \to \Psh{\Delta}
\]
\fi
%Note that since (small) categories can be seen as $\oo$-categories with only unit cells above dimension $1$, we can restrict the functor to $N_{\omega}$ to the category of small categories $\Cat$, seen as a full subcategory of $\oo\Cat$. In this case, $N_{\omega}\vert_{\Cat}$ coincide with the usual nerve of (small) categories and Thomason weak equivalences between small categories are exactly the weak equivalences of the model structure on $\Cat$ established  

\iffalse
Before entering the heart of the subject and explaining precisely what the above means, let us quickly linger on a terminological detail concerning strict $\oo$-category. In this flavour of higher category theory, the axioms for compositions and units hold strictly, which means that they are witnessed by genuine equalities and not only up to higher dimensional cells. On the other hand, \emph{no} invertibility axioms is required on any dimension. In particular, strict $\oo$-categories are \emph{not} a particular case of $(\infty,1)$-categories in the sense of Lurie (improperly called $\infty$-categories).
\fi



\iffalse
In this vast generalization of category theory, the objects of study have objects and arrows, as for categories, but also arrows between arrows, called $2$-arrows or $2$-cells, arrows between arrows between arrows ($3$-cells), and so on. All these data come equipped with various composition laws between cells. Higher categories come in many different flavours which loosely depend on three main parameters:
\begin{itemize}[label=-]
\item The maximum dimension of the cells. One then speak of $1$-categories (which is a synonym for the usual categories), $2$-categories, $3$-catgegories and so on.
  \item The requirement that units and associativity axioms for compositions either are witnessed by genuine equalities  
  \end{itemize}
\fi

%%%%% 3rd oriental

\begin{tikzcd}[column sep=huge,row sep=huge]
  \langle 0 \rangle \ar[r,"\langle 01 \rangle"description] \ar[d,"\langle 13 \rangle"description] & \langle 1 \rangle \ar[d,"\langle 02 \rangle"description]\\
  \langle 3 \rangle & \langle 3  \rangle \ar[l,"\langle 23 \rangle" description] 
  \ar[from=1-1,to=2-2,"\langle 02 \rangle" description,""{name=A,above}]
  \ar[from=1-2,to=A,Rightarrow,"\langle 013 \rangle"description]
  \end{tikzcd}
  \overset{\langle 0123 \rangle}{\Rrightarrow}
  \begin{tikzcd}[column sep=huge,row sep=huge]
      \langle 0 \rangle \ar[r,"\langle 01 \rangle"description] \ar[d,"\langle 13 \rangle"description] & \langle 1 \rangle \ar[d,"\langle 02 \rangle"description]\\
  \langle 3 \rangle & \langle 3  \rangle \ar[l,"\langle 23 \rangle" description] 
  \ar[from=1-2,to=2-1,"\langle 12 \rangle" description]
  \end{tikzcd}
  \]

  %%%% comparison homology

    and then a morphism of op-prederivators
     \[\J : \Ho(\oo\Cat^{\folk}) \to \Ho(\oo\Cat^{\Th}).\]
     This yields the following triangle, 
     \begin{equation}\label{cmprisontrngle}
     \begin{tikzcd}
       \Ho(\oo\Cat^{\folk}) \ar[r,"\J"] \ar[rd,"\sH^{\pol}"'] & \Ho(\oo\Cat^{\Th}) \ar[d,"\sH"] \\
      & \Ho(\Ch).
     \end{tikzcd}
     \end{equation}
     As we shall see later, this triangle is \emph{not} commutative, even up to an iso. However, it can be filled up with a $2$-morphism. Indeed, consider the following $2$-diagram
     \[
     \begin{tikzcd}
       \oo\Cat \ar[r,"\mathrm{id}_{\oo\Cat}"]\ar[d] & \oo\Cat \ar[d] \ar[r,"\lambda"] & \Ch \ar[d] \\
       \Ho(\oo\Cat^{\folk}) \ar[r] &\ho(\oo\Cat^{\Th}) \ar[r,"\sH"] & \Ho(\Ch),
       \ar[from=2-2,to=1-3,"\alpha^{\Th}",Rightarrow]
     \end{tikzcd}
     \]
     where the left square is commutative and the $2$-morphism on the right square is the one from Paragraph \ref{paragr:univmor}. Since the polygraphic homology is, by definition, the left derived of the abelianization functor when $\oo\Cat$ is equipped with canonical weak equivalences, the universal property of strongly derivable functor yields a canonical $2$-morphism
     \begin{equation}\label{cmparisonmapdiag}
     \begin{tikzcd}
       \Ho(\oo\Cat^{\folk}) \ar[r] \ar[rd,"\sH^{\pol}"',""{name=A,above}] & \Ho(\oo\Cat^{\Th}) \ar[d,"\sH"] \\
      & \Ho(\Ch)\ar[from=1-2,to=A,"\pi",Rightarrow]
     \end{tikzcd}
     \end{equation}
     such that the triangle
     \[
     \begin{tikzcd}
       \lambda \ar[r] \ar[dr] & \sH \J \ar[d] \\
       & \sH^{\pol}
     \end{tikzcd}
     \]
     is commutative \todo{Compléter diagramme précédent.}
     \iffalse
     \[
     \begin{tikzcd}
       \oo\Cat \ar[r,"\mathrm{id}_{\oo\Cat}"]\ar[d] & \oo\Cat \ar[d] \ar[r,"\lambda"] & \Ch \ar[d] \\
       \Ho(\oo\Cat^{\folk}) \ar[r]&\Ho(\oo\Cat^{\Th}) \ar[r,"\sH"] & \Ho(\Ch),
       \ar[from=2-2,to=1-3,"\alpha^{\Th}",Rightarrow]
     \end{tikzcd}
     =
     \begin{tikzcd}
       \oo\Cat \ar[r,"\lambda"] \ar[d] & \Ch \ar[d] \\
       \Ho(\oo\Cat) \ar[r,"\sH^{\pol}"]
     \end{tikzcd}
     \]\fi
     The $2$-morphism \ref{cmparisonmapdiag} induces a natural transformation at the level of localized \emph{categories}
     \[
     \begin{tikzcd}
       \ho(\oo\Cat^{\folk}) \ar[r] \ar[rd,"\sH^{\pol}"',""{name=A,above}] & \ho(\oo\Cat^{\Th}) \ar[d,"\sH"] \\
      & \ho(\Ch)\ar[from=1-2,to=A,"\pi",Rightarrow]
     \end{tikzcd}
     \]
     Since $\J$ is nothing but the identity on objects, for any $\oo$\nbd-category $X$, the previous natural transformation yields a map 
     \[
     \pi_X : \sH(X) \to \sH^{\pol}(X),
     \]
     which we shall refer to as the \emph{canonical comparison map.}
   
     %%%% Canonical comparison map
         
    \[
     \begin{tikzcd}
       \lambda \ar[r,Rightarrow] \ar[dr,Rightarrow] & \sH^{\sing} \circ \J \ar[d,Rightarrow] \\
       & \sH^{\pol}
     \end{tikzcd}
     \]
     is commutative
     \iffalse
     \[
     \begin{tikzcd}
       \oo\Cat \ar[r,"\mathrm{id}_{\oo\Cat}"]\ar[d] & \oo\Cat \ar[d] \ar[r,"\lambda"] & \Ch \ar[d] \\
       \Ho(\oo\Cat^{\folk}) \ar[r]&\Ho(\oo\Cat^{\Th}) \ar[r,"\sH"] & \Ho(\Ch),
       \ar[from=2-2,to=1-3,"\alpha^{\Th}",Rightarrow]
     \end{tikzcd}
     =
     \begin{tikzcd}
       \oo\Cat \ar[r,"\lambda"] \ar[d] & \Ch \ar[d] \\
       \Ho(\oo\Cat) \ar[r,"\sH^{\pol}"]
     \end{tikzcd}
     \]\fi
     The $2$-morphism \ref{cmparisonmapdiag} induces a natural transformation at the level of localized \emph{categories}
     \[
     \begin{tikzcd}
       \ho(\oo\Cat^{\folk}) \ar[r] \ar[rd,"\sH^{\pol}"',""{name=A,above}] & \ho(\oo\Cat^{\Th}) \ar[d,"\sH"] \\
      & \ho(\Ch)\ar[from=1-2,to=A,"\pi",Rightarrow]
     \end{tikzcd}
     \]


     %%%% CLASSIFYING BUBBLES
     
      In fact, this $2$\nbd{}category classifies bubbles in the
  sense that the functor
  \begin{align*}
    2\Cat &\to \Set \\
    C &\mapsto \Hom_{2\Cat}(B^2\mathbb{N},C)
  \end{align*}
  is canonically isomorphic to the functor that sends a $2$\nbd{}category to its
  set of bubbles.

  \todo{À finir}