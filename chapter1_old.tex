\documentclass[12pt,a4paper]{report}

\usepackage{mystyle}

\begin{document}
\chapter{Yoga of $\oo$-Categories : OLD}
\section{$n$-graphs, $n$-magmas and $n$-categories}

\begin{paragr}\label{pargr:defngraph}
   Let $n \in \mathbb{N}$. An \emph{$n$-graph} $C$ consists of a finite sequence $(C_k)_{0\leq k \leq n}$ of sets together with maps
  \[ \begin{tikzcd}
    C_{k-1} &\ar[l,"s",shift left] \ar[l,"t"',shift right] C_{k}
  \end{tikzcd}
  \]
  for every $0 < k \leq n$, subject to the \emph{globular identities}:
  \begin{equation*}
  \left\{
  \begin{aligned}
    s \circ s &= s \circ t, \\
    t \circ t &= t \circ s.
  \end{aligned}
  \right.
  \end{equation*}
  For example, a $0$-graph is just a set and a $1$-graph is an ordinary graph:
    \[
  \begin{tikzcd}
    C_0 & \ar[l,shift right,"t"'] \ar[l,shift left,"s"] C_1.
    \end{tikzcd}
  \]
 Elements of $C_k$ are called \emph{$k$-cells} or \emph{cells of dimension $k$}.
  For $x$ a $k$-cell with $k>0$, $s(x)$ is the \emph{source} of $x$ and $t(x)$ is the \emph{target} of $x$.
  
  More generally for all $k, l\in \mathbb{N}$ such that $k < l \leq n$, we define maps $s_k,t_k : C_l \to C_k$ as
  \[
  s_k = \underbrace{s\circ \dots \circ s}_{n-k \text{ times}}
  \]
  and
    \[
  t_k = \underbrace{t\circ \dots \circ t}_{n-k \text{ times}}.
  \]
  For an $l$-cell $x$, $s_k(x)$ is the \emph{$k$-source} of $x$ and $t_k(x)$ the \emph{$k$-target} of $x$.


  Two $k$-cells $x$ and $y$ are \emph{parallel} if $k=0$ or $k>0$ and
  \[
  s(x)=s(y) \text{ and } t(x)=t(y).
  \]
   For all $k, l\in \mathbb{N}$ such that $k < l \leq n$, we define the set $C_l\underset{C_k}{\times}C_l$ as the following fibred product
    \[
    \begin{tikzcd}
      C_l\underset{C_k}{\times}C_l \ar[r] \ar[dr,phantom,"\lrcorner", very near start] \ar[d] &C_l \ar[d,"t_k"]\\
      C_l \ar[r,"s_k"] & C_k.
      \end{tikzcd}
    \]
    That is, elements of $C_l\underset{C_k}{\times}C_l$ are pairs $(x,y)$ of $l$-cells such that $s_k(x)=t_k(y)$. We say that two $l$-cells $x$ and $y$ are \emph{$k$-composable} if the pair $(x,y)$ belongs to $C_l\times_{C_k}C_l$. 

  Let $C$ and $C'$ be two $n$-graphs. A \emph{morphism of $n$-graphs} $f : C \to C'$ is a sequence of maps $(f_k : C_k \to D_k)_{0 \leq k \leq n}$  such that for every $0< k \leq n$, the squares
    \[
    \begin{tikzcd}
    C_k \ar[d,"s"] \ar[r,"f_k"]&C'_k \ar[d,"s"] \\
    C_{k-1} \ar[r,"f_{k-1}"] & C'_{k-1}
    \end{tikzcd}
\quad
        \begin{tikzcd}
    C_k \ar[d,"t"] \ar[r,"f_k"]&C'_k \ar[d,"t"] \\
    C_{k-1} \ar[r,"f_{k-1}"] & C'_{k-1}
    \end{tikzcd}
    \]
    are commutative.

    For a $k$-cell $x$, we will often write $f(x)$ instead of $f_k(x)$.
    We denote by $\nGrph$ the category of $n$-graphs and morphisms of $n$-graphs.

     For any $n>0$, there is an obvious ``truncation'' functor
     \[
       \tau : n\Grph \to (n\shortminus 1)\Grph
  \]
  that simply forgets the $n$-cells. That is to say, for $C$ an $n$-graph, $\tau(C)$ is the $(n \shortminus 1)$-graph with $\tau(C)_k=C_k$ for every $0 \leq k \leq n\shortminus 1$. 
    \end{paragr}

  \begin{paragr}
  Let $n \in \mathbb{N}$. An \emph{$n$-magma} consists of:
  \begin{itemize}
  \item[-] an $n$-graph $C$,
  \item[-] maps
    \[
    \begin{aligned}
    (\shortminus)\underset{k}{\ast}(\shortminus)  : C_l\underset{C_k}{\times}C_l &\to C_l \\
      (x,y) &\mapsto x\underset{k}{\ast}y
      \end{aligned}
    \]
    for all $l,k \in \mathbb{N}$ with $k < l \leq n$,
  \item[-] maps
    \[
    \begin{aligned}
    1_{(\shortminus)} : C_k &\to C_{k+1}\\
      x &\mapsto 1_x
      \end{aligned}
    \]
    for every $k \in \mathbb{N}$ with $k< n$,
  \end{itemize}
  subject to the following axioms:
  \begin{enumerate}[label=(\alph*)]
  \item For all $k,l \in \mathbb{N}$ with $k<l\leq n$ and every $k$-composable $l$-cells $x$ and $y$,
    \[
    s(x\underset{k}{\ast} y) =
    \begin{cases}
      s(y) &\text{ when }k=l-1,\\
      s(x)\underset{k}{\ast} s(y) &\text{ otherwise,}
      \end{cases}
    \]
    and
        \[
    t(x\underset{k}{\ast} y) =
    \begin{cases}
      t(x) & \text{ when }k=l-1,\\
      t(x)\underset{k}{\ast} t(y) &\text{ otherwise.}
      \end{cases}
    \]
  \item For every $k \in \mathbb{N}$ with $k\leq n$ and every $k$-cell,
    \[
    s(1_x)=t(1_x)=x.
    \]
  \end{enumerate}
  We will use the same letter to denote an $n$-magma and its underlying $n$-graph.
  
   For two $k$-composable $l$-cells $x$ and $y$, we refer to $x\ast_ky$ as the \emph{$k$-composition} of $x$ and $y$.
   For a $k$-cell $x$, we refer to $1_{x}$ as the \emph{unit on $x$}.
   
      \remtt{Je n'aime pas trop les notations et définitions des unités itérées qui suivent.}
    More generally, for any $l \in \mathbb{N}$ with $k < l\leq n$, we define $\1^{l}_{(\shortminus)} : C_k \to C_l$ as
    \[
    \1^{l}_{(\shortminus)} := \underbrace{1_{(\shortminus)} \circ \dots \circ 1_{(\shortminus)}}_{l-k \text{ times }} : C_k \to C_l.
    \]
    Let $x$ be a $k$-cell, $\1^l_x$ is the \emph{$l$-dimensional unit on $x$}, and for consistency, we also set
    \[
    \1^{k}_x := x.
    \]
    A cell is \emph{degenerate} or \emph{trivial} if it is a unit on a strictly lower dimensional cell.

    
    Let $C$ and $C'$ be $n$-magmas. A \emph{morphism of $n$-magmas} $f : C \to C'$ is a morphism of $n$-graphs that is compatible with compositions and units. This means:
  \begin{itemize}
  \item[-]for all $k,l \in \mathbb{N}$ with $k<l\leq n$ and every $k$-composable $l$-cells $x$ and $y$,
    \[
    f(x\underset{k}{\ast}y)=f(x)\underset{k}{\ast}f(y),
    \]
  \item[-]for every $k \in \mathbb{N}$ with $k\leq n$ and every $k$-cell $x$,
    \[
    f(1_x)=1_{f(x)}.
    \]
  \end{itemize}
  We denote by $\nMag$ the category of $n$-magmas and morphisms of $n$-magmas.

      For any $n>0$, there is an obvious ``truncation'' functor
    \[
    \tau : n\Mag \to (n\shortminus 1)\Mag
    \]
    that simply forgets the $n$-cells. Moreover, the square
    \[
    \begin{tikzcd}
      n\Mag \ar[r,"\tau"]\ar[d] & (n\shortminus 1)\Mag \ar[d]\\
      n\Grph \ar[r,"\tau"] & (n\shortminus 1)\Grph
      \end{tikzcd}
    \]
    where the vertical arrows are the obvious forgetful functors, is commutative.
\end{paragr}
 
\begin{paragr}
  Let $n \in \mathbb{N}$. An \emph{$n$-category} $C$ is an $n$-magma such that the following axioms are satisfied:
  \begin{enumerate}[label=(\textbf{CAT}\alph*)]
    \item for all $k,l \in \mathbb{N}$ with $k<l\leq n$, for all $k$-composable $l$-cells $x$ and $y$, we have 
    \[
    1_{x\underset{k}{\ast}y}=1_{x}\underset{k}{\ast}1_{y},
    \]
  \item for all $k,l \in \mathbb{N}$ with $k<l \leq n$, for every $l$-cell $x$, we have
    \[
    \1^l_{t_k(x)}\comp_k x =x= x \comp_k\1^l_{s_k(x)},
    \]
  \item for all $k,l \in \mathbb{N}$ with $k<l\leq n$, for all $l$-cells $x, y$ and $z$ such that $x$ and $y$ are $k$-composable, and $y$ and $z$ are $k$-composable, we have
    \[
    (x\comp_{k}y)\comp_{k}z=x\comp_k(y\comp_kz),
    \]
    \item for all $k, l \in \mathbb{N}$, for all $n$-cells $x,x',y$ and $y'$  such that
    \begin{itemize}
    \item[-] $x$ and $y$ are $l$-composable, $x'$ and $y'$ are $l$-composable,
    \item[-] $x$ and $x'$ are $k$-composable, $y$ and $y'$ are $k$-composable,
    \end{itemize}
    we have
    \[
    ((x \comp_k x')\comp_l (y \comp_k y'))=((x \comp_l y)\comp_k (x' \comp_l y')).
    \]
  \end{enumerate}
  We will use the same letter to denote an $n$-category and its underlying $n$-magma. Let $C$ and $C'$ be $n$-categories, a \emph{morphism of $n$-categories} (or $n$-functor) $f : C \to C'$ is simply a morphism of $n$-magmas. We denote by $n\Cat$ the category of $n$-categories and morphisms of $n$-categories. 
\end{paragr}
\begin{paragr}
  Let $n>0$. As in the case of $n$-graphs and $n$-magmas, there is an obvious ``truncation'' functor
  \[
  \tau : n\Cat \to (n\shortminus 1)\Cat
  \]
  that simply forgets the $n$-cells and the square
  \[
  \begin{tikzcd}
  n\Cat \ar[r,"\tau"]\ar[d] & (n\shortminus 1)\Cat\ar[d]\\
  n\Mag \ar[r,"\tau"] & (n\shortminus 1)\Mag
  \end{tikzcd}
  \]
  where the vertical arrows are the obvious forgetul functors, is commutative.
  Now let $C$ be an $(n\shortminus 1)$-category. We define an $n$-category $\iota(C)$ with
  \begin{itemize}
  \item[-] $\tau(\iota(C))=C$,
  \item[-] $\iota(C)_{n}=C_{n-1}$,
  \item[-] source and targets maps $\iota(C)_{n} \to \iota(C)_{n-1}$ as the identity,
  \item[-] unit map $\iota(C)_{n-1} \to \iota(C)_n$ as the identity,
    \item[-] for every $k<n$, composition map $\iota(C)_n\underset{\iota(C)_k}{\times}\iota(C)_n \simeq \iota(C)_n \to \iota(C)_n$ as the identity.
    \end{itemize}
  It is immediate to see that $\iota(C)$ is indeed an $n$-category and the correspondance $ C \mapsto \iota(C)$ can canonically be made into a functor:
  \[
    \iota : (n\shortminus1)\Cat \to n\Cat.
    \]
 %   andy definition, for any $(n \shortminus 1)$-category, we have
  %  \[
  %  \tau(\iota(C))=C.
  %  \]

    We also define an $n$-category $\kappa(C)$ with
    \begin{itemize}
    \item[-]$\tau(\kappa(C))=C$,
    \item[-]$\kappa(C)_n=\left\{(a,b) \in C_{n-1}\times C_{n-1} \vert a \text{ and } b \text{ are parallel}\right\}$,
    \item[-]for $(a,b) \in \kappa(C)_n$,
      \[
      t((a,b))=a \text{ and } s((a,b))=b,
      \]
    \item[-] for $(a,b)$ and $(b,c) \in \kappa(C)_n$,
      \[
      (a,b)\comp_{n-1}(b,c) = (a,c),
      \]
    \item[-] for $(a,b)$ and $(a',b')  \in \kappa(C)_n$ that are $k$-composable with $k<n-1$,
      \[
      (a,b)\comp_k(a',b') = (a\comp_k a',b\comp_k b').
      \]
      (The right hand side makes sense since $a$ and $b$ are parallel and, $a'$ and $b'$ are parallel.)
    \end{itemize}
    It is straightforward to check that $\kappa(C)$ is indeed an $n$-category and the correspondance $C \mapsto \kappa(C)$ can canonically be made into a functor:
    \[
    \kappa : (n\shortminus 1)\Cat \to n\Cat.
    \]
\end{paragr}
\begin{lemma}
  We have a sequence of adjunctions
  \[
  \iota \dashv \tau \dashv \kappa.
  \]
  Moreover, the unit of the adjunction $\iota \dashv \tau$ is the equality:
  \[
  \mathrm{id}_{(n\shortminus 1)\Cat}=\tau \circ \iota,
  \]
  and the co-unit of the right adjunction $\tau\dashv \kappa$ is the equality: 
    \[
  \tau \circ \kappa = \mathrm{id}_{(n\shortminus 1)\Cat}.
  \]
\end{lemma}
\begin{proof}
  \todo{À écrire.}
\end{proof}
\begin{remark}
  The ``truncation'' functors $\tau : n\Mag \to (n\shortminus 1)\Mag$ and $\tau : n\Grph \to (n\shortminus 1)\Grph$ also have left and right adjoints but we won't need them in the sequel.
\end{remark}
\section{Bases for $n$-categories}
\begin{paragr}
  Let $C$ be an $n$-category and $k\in \mathbb{N}$ with $0\leq k \leq n$. A subset of $k$-cells
  \[
  \Sigma \subseteq C_k
  \]
  is \emph{saturated} if $k=0$ and $\Sigma = C_0$, or $k>0$ and:
  \begin{itemize}
  \item[-] for every $x \in C_{k\shortminus 1}$, we have
    \[
    1_x \in \Sigma,
    \]
  \item[-] for all $x, y \in \Sigma$ that are $l$-composable with $l<k$, we have
    \[
    x\comp_ly \in \Sigma.
    \]
  \end{itemize}
  It is immediate to see that any intersection of saturated subset of $k$-cells is again satured. For any arbitrary subset of $k$-cells $\Sigma \subseteq C_k$, we denote by $\overline{\Sigma}$ the smallest saturated subset of $k$-cells that contains $\Sigma$.
  \end{paragr}
  \begin{definition}
    Let $C$ be an $n$-category and $k\in \mathbb{N}$ with $0\leq k \leq n$. We say that subset of $k$-cells $\Sigma \subseteq C_k$ is a \emph{$k$-prebasis} when
    \[
    \overline{\Sigma}=C_k.
    \]
    \end{definition}
  \begin{definition}
    
  \end{definition}
  \section{Generating cells}
  \begin{paragr}
    A \emph{cellular extension} of an $n$-magma $M$ in the data of a quadruple $(\Sigma,M,s,t)$ where $\Sigma$ is a set, $M$ is a $n$-magma, $s$ and $t$ are maps
    \[
    s,t : \Sigma \to M_n
    \]
    such that
    \[
    \begin{tikzcd}
      M_{n-1} & \ar[l,shift right, "s"'] \ar[l,shift left,"t"] M_n & \ar[l,shift right, "s"'] \ar[l,shift left,"t"] \Sigma
      \end{tikzcd}
    \]
    satisfy the globular identities. We will often use the abusive notation $(\Sigma,M)$ instead of $(\Sigma,M,s,t)$. 
  \end{paragr}
\begin{paragr}
  Let $E=(\Sigma,M,s,t)$ be an cellular extension of an $n$-magma. Consider the alphabet that has:
  \begin{itemize}
  \item[-] a symbol $\hat{x}$ for each $x \in \Sigma$,
  \item[-] a symbol $\fcomp_k$ for each $k<n$,
  \item[-] a symbol $\ii_x$ for each $x \in C_{n\shortminus 1}$,
  \item[-] a symbol of opening parenthesis $($,
    \item[-] a symbol of closing parenthesis $)$.
  \end{itemize}
  We denote by $\W[\Sigma]$ the set of words on this alphabet (i.e. finite sequence of symbols). If $w$ and $w'$ are elements of $\mathcal{W}[\Sigma]$, we write $ww'$ for their concatenation.

%The \emph{length} of a word $w$, denoted by $\mathcal{L}(w)$, is the number of symbols that appear in $w$.
We now recursively define the set $\Sigma^{+} \subseteq \W[\Sigma]$ of \emph{well formed words} on this alphabet together with maps $s,t : \Sigma^{+}  \to M_{n-1}$:
\begin{itemize}
\item[-] for every $x \in \Sigma$, we have $\hat{x} \in \Sigma^{+}$  with
  \[s(\hat{x})=s(x) \text{ and }t(\hat{x})=t(x),\]
\item[-] for every $x \in C_n$, we have $\ii_{x} \in \Sigma^{+}$ with
  \[s(\ii_x)=t(\ii_x)=x,\]
\item[-] for all $v,w \in \Sigma^{+}$ such that $s(v)=t(w)$, we have $ (v \fcomp_n w) \in \Sigma^{+}$ with \[s((v \fcomp_n w))=s(w)\text{ and }t((v \fcomp_n w))=t(v),\]
\item[-] for all $v, w \in \Sigma^{+}$ and $0 \leq k < n\shortminus 1$, such that $s_k(s(v))=t_k(t(w))$, we have $(v \fcomp_k w) \in \Sigma^{+}$ with \[s((v \hat{\comp_k} w)) = s(v) \comp_k s(w)\] and \[t((v \hat{\comp_k} w))=t(v)\comp_k t(w).\]
  \end{itemize}
  We define $s_k , t_k: \Sigma^{+} \to M_k$ as iterated source and target (with $s_n=s$ and $t_n=t$ for consistency). We say that two well formed words $v$ and $w$ are \emph{parallel} if
\[s(v)=s(w) \text{ and }t(v)=t(w).\]
and we say that they are \emph{$k$-composable} for a $k< n$ if
\[s_k(v)=t_k(w).\]
%Let $E'=(\Sigma',C',s',t')$ be another $n$-cellular extension and $(\varphi,f) : E \to E'$ a morphism of $n$-cellular extensions. We recursively define a map $f^+ : \Sigma^+ \to \Sigma^+$ with
%\begin{itemize}
%  \item[-]
%\end{itemize}
\end{paragr}
\begin{paragr}
  Let $C$ be an $n$-category, $k \in \mathbb{N}$ with $0<k \leq n$ and $\Sigma \subseteq C_k$ a subset of the $k$-cells of $C$ with $k\leq n$. It defines an cellular extension $(\Sigma,\tau_{k-1}(C),\sigma,\tau)$ of $\tau_{k-1}(C)$, where $s$ and $t$ are simply the restriction of the source and target maps $C_k \to C_{k-1}$. The canonical inclusion
  \[
  \Sigma \hookrightarrow C_k
  \]
  is recursively extended to a map $\rho : \Sigma^{+} \to C_k$ in the following way:
  \begin{enumerate}[label=-]
  \item $\rho(\hat{x})=x$ for every $x \in \Sigma$,
    \item $\rho((v\fcomp_kw))=\rho(v)\comp_k\rho(w)$.
  \end{enumerate}
  We call $\rho$ the \emph{evaluation map}. Intuitively, this maps ``evaluates'' the formal expressions constructed out of the cells in $\Sigma$ into actual $k$-cells of $\Sigma$. 
\end{paragr}
\begin{definition}
  Let $C$ be an $n$-category and $k\leq n$. A subset $\Sigma \subseteq C_k$ of $k$-cells \emph{generates by composition} if the evaluation map
  \[
  \rho : \Sigma^+ \to C_k
  \]
  is surjective.
\end{definition}
\begin{definition}
  Let $M$ be an $n$-magma and $k \leq n$. A \emph{congruence} on the $k$-cells of $M$ is a binary relation $\R$ on $M_k$ such that
  \begin{enumerate}[label=-]
  \item $\R$ is an equivalence relation,
  \item if $x\R y$ then $x$ and $y$ are parallel,
  \item for $x, x', y, y' \in M_k$ such that $x$ and $x'$ are $l$-composable, $y$ and $y'$ are $l$-composable if $x\R y$ and $x'\R y'$, then $(x\ast_l y )\R (x'\ast_l y')$.  
  \end{enumerate}
\end{definition}
\begin{lemma}
  Let $M$ be an $n$-magma and $(\R_i)_{i \in I}$ a family of congruence on the $k$-cells of $M$. If $I$ is non-empty then $\cap_{i \in I} \R_i$ is a congruence on the $k$-cells of $M$.
\end{lemma}
\
\section{$\oo$-categories}
\begin{paragr}
  For any $n>0$, there is an obvious ``truncation'' functor
  \[
  \nGrph \to (n\shortminus 1)\Grph
  \]
  that simply forgets the $n$-cells. We define the category $\oo\Grph$ of $\omega$-graphs as the limit of the diagram
  \[
  \cdots \to \nGrph \to (n\shortminus 1)\Grph \to \cdots \to 1\Grph \to 0\Grph.
  \]
  \end{paragr}
  \begin{remark}
    More concretely, the definitions of $\omega$-graph and morphism of $\omega$-graphs are the same as the definitions of $n$-graph and morphisms of $n$-graphs with $n$ finite but with the condition ``$\leq n$'' dropped everywhere. For example, an $\omega$-graph has an infinite sequence of cells $(C_k)_{k\in \mathbb{N}}$.
    \remtt{Est-ce que je devrais plus détailler la définition de $\oo$-graphe ?}
  \end{remark}
 \begin{paragr}
    For any $n>0$, there is an obvious ``truncation'' functor
    \[
    n\Mag \to (n\shortminus 1)\Mag
    \]
    that simply forgets the $n$-cells. We define the category $\oo\Mag$ of $\omega$-magmas as the limit of the diagram
    \[
     \cdots \to n\Mag \to (n\shortminus 1)\Mag \to \cdots \to 1\Mag \to 0\Mag.
     \]
     Moreover, for any $n\in \mathbb{N}$, there is a canonical forgetful functor
     \[
     n\Mag \to n\Grph
     \]
     such that when $n>0$ the square
     \[
     \begin{tikzcd}
       n\Mag\ar[r] \ar[d] & n\Grph \ar[d]\\
       (n \shortminus 1)\Mag \ar[r] & (n \shortminus 1)\Grph
       \end{tikzcd}
     \]
     is commutative. By universal property, this yields a canonical forgetful functor
     \[
     \oo\Mag \to \oo\Grph.
     \]
    \end{paragr}
\begin{paragr}\label{paragr:defoocat}
  Once again, for any $n>0$ there is a canonical ``truncation'' functor
  \[
  n\Cat \to (n \shortminus 1)\Cat
  \]
  that simply forgets the $n$-cells. We define the category $\oo\Cat$ of $\oo$-categories as the limit of the diagram
  \[
     \cdots \to n\Cat \to (n\shortminus 1)\Cat \to \cdots \to 1\Cat \to 0\Cat.
     \]
     By definition, for every $n \in \mathbb{N}$, there is a canonical forgetful functor
     \[
     n\Cat \to n\Mag,
     \]
     which is full. When $n>0$, the canonical square
     \[
     \begin{tikzcd}
       n\Cat\ar[r] \ar[d] & n\Mag \ar[d]\\
       (n \shortminus 1)\Cat \ar[r] & (n \shortminus 1)\Mag
       \end{tikzcd}
     \]
     is commutative. By universal property, this yields a canonical forgetful functor
     \[
     \oo\Cat \to \oo\Mag,
     \]
     which is easily seen to be full. 
\end{paragr}
\remtt{Le lemme suivant est un genre d'Eilenberg-Zilber pour les oo-catégories.}
\begin{lemma}\label{EZoocat}
  Let $C$ be an $\oo$-category and $n \in \mathbb{N}$. For any $n$-cell $x$ of $C$, there exist a unique non-degenerate $k$-cell $x'$ with $k\leq n$ such that
  \[
  \1^n_{x'}=x.
  \]
\end{lemma}
\begin{proof}
  \todo{Évident. À écrire ?}
  \end{proof}
\begin{paragr}
  For any $n>0$, let us denote $\tau$ the canonical truncation functor
  \[
  \tau : n\Cat \to (n \shortminus 1)\Cat
  \]
  from Paragraph \ref{paragr:defoocat}.

  Let $C$ be a $(n\shortminus 1)$-category. We define an $n$-magma $\iota(C)$ with
  \begin{itemize}
  \item[-] $\tau(\iota(C))=C$,
  \item[-] $C'_{n}=C_{n-1}$,
  \item[-] source and targets maps $\iota(C)_{n} \to \iota(C)_{n-1}$ as the identity,
  \item[-] unit map $\iota(C)_{n-1} \to \iota(C)_n$ as the identity,
    \item[-] for every $k<n$, composition map $\iota(C)_n\underset{\iota(C)_k}{\times}\iota(C)_n \simeq \iota(C)_n \to \iota(C)_n$ as the identity.
    \end{itemize}
  It is immediate to see that $\iota(C)$ is in fact an $n$-category and the correspondance $ C \mapsto \iota(C)$ can canonically made into a functor:
  \[
    \iota : (n\shortminus1)\Cat \to n\Cat.
    \]
 %   By definition, we have
 %   \[
 %   \tau \circ \iota = \mathrm{id}_{(n\shortminus 1)\Cat}.
 %   \]
    More generally, for all $n>k \in \mathbb{N}$, we define $\iota^n_k : k\Cat \to n\Cat$ as the composition
    \[
    \iota^n_k = \underbrace{\iota \circ \cdots \circ\iota}_{n-k \text{ times}}.
    \]
    For any $n \in \mathbb{N}$, the commutative diagram
    \[
    \begin{tikzcd}
      \cdots \ar[r]&(n+3)\Cat \ar[r,"\tau"] & (n+2)\Cat \ar[r,"\tau"] &(n+1)\Cat\\
      &&&\\
      n\Cat \ar[uur,"\iota^{n+3}_n"] \ar[uurr,"\iota^{n+2}_n"] \ar[uurrr,"\iota^{n+1}_n"'] &&&
      \end{tikzcd}
    \]
    induces by universal property a functor
    \[
    \iota^n : n\Cat \to \oo\Cat.
    \]
    Let us denote by
  \[
  \tau_{\leq n} : \oo\Cat \to n\Cat
  \]
  the canonical arrow of the limiting cone.
  
  \todo{Montrer qu'on a une adjonction $\iota_n \dashv \tau_{\leq n}$}
  
  Let $C$ be an $\oo$-category. We define the \emph{$n$-skeleton of $C$} as the $\oo$-category
  \[
  \sk_n(C):= \iota^n(\tau_{\leq n} (C)).
  \]
  There is a canonical diagram in $\oo\Cat$ \todo{Comment on le définit ?}
  \[
  \begin{tikzcd}[column sep=small]
    \sk_0(C) \ar[r] & \sk_1(C) \ar[r] & \cdots \ar[r] & \sk_n(C) \ar[r] &\sk_{n+1}(C) \ar[r] & \cdots
    \end{tikzcd}
  \]
  that we call the \emph{canonical filtration of $C$} and the sequence of arrows
  \[
(\eta_n : \sk_n(C) \to C)_{n \in \mathbb{N}}
  \]
  where $\eta_n$ is the unit of the adjunction $\iota^n \dashv \tau_n$, defines a cocone on the previous diagram.
\end{paragr}
\begin{lemma}
  Every $\oo$-category is the colimit of its canonical filtration. 
\end{lemma}
\begin{proof}
  \todo{...}
  test
  \end{proof}
\end{document}
