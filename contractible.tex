\chapter{Homology of contractible \texorpdfstring{$\oo$}{ω}-categories and its
  consequences}
\chaptermark{Contractible $\omega$-categories and consequences}
\section{Contractible \texorpdfstring{$\oo$}{ω}-categories}
Recall that for every $\oo$\nbd{}category $C$, we write $p_C : C \to \sD_0$ for
the canonical morphism to the terminal object $\sD_0$ of $\oo\Cat$.
\begin{definition}\label{def:contractible}
  An $\oo$\nbd{}category $C$ is \emph{oplax contractible} when the canonical morphism $p_C : C \to \sD_0$ is an oplax homotopy equivalence (Definition \ref{def:oplaxhmtpyequiv}).

  %% there exists an object $x_0$ of $X$ and an oplax transformation
  %% \[
  %% \begin{tikzcd}
  %%   X \ar[r,"p_X"] \ar[rd,"1_X"',""{name=A,above}]& \sD_0 \ar[d,"\langle x_0 \rangle"]\\
  %%   &X.
  %%   \ar[from=A,to=1-2,"\alpha",Rightarrow]
  %% \end{tikzcd}
  %% \]
\end{definition}


%% \begin{paragr}
%%   In other words, an $\oo$\nbd{}category $X$ is contractible when there exists an object $x_0$ of $X$ such that $\langle x_0 \rangle : \sD_0 \to X$ is a deformation retract (Paragraph \ref{paragr:defrtract}). It follows from Lemma \ref{lemma:oplaxloc} that $p_X : X \to \sD_0$ is a Thomason equivalence. In particular, we have the following lemma.
%% \end{paragr}

%% \begin{lemma}\label{lemma:hmlgycontractible} 
%%   Let $X$ be a contractible $\oo$\nbd{}category. The morphism of $\ho(\Ch)$
%%   \[
%%   \sH(X) \to \sH(\sD_0)
%%   \]
%%   induced by the canonical morphism $p_X : X \to \sD_0$ is an isomorphism.
%% \end{lemma}
%% \begin{paragr}
%%   In addition to the previous result, it is immediate to check that $\sH(\sD_0)$ is nothing but $\mathbb{Z}$ seen as an object of $\ho(\Ch)$ concentrated in degree $0$. 
%% \end{paragr}
%% \begin{corollary}
%%   Let $u : X \to Y$ be a morphism of $\oo\Cat$. If $u$ is a homotopy equivalence (Paragraph \ref{paragr:hmtpyequiv}), then
%%   \[
%%   \sH^{\pol}(\gamma_{\folk}(u))
%%   \]
%%   is an isomorphism.
%% \end{corollary}
%% \todo{Expliquer p-e pourquoi le corollaire précédent est important.}
%% \begin{corollary}\label{cor:hmlgypolcontractible}
%%   Let $X$ be an $\oo$-category. If $X$ is contractible, then the morphism
%%   \[
%%   \sH^{\pol}(\gamma_{\folk}(p_X)) : \sH^{\pol}(X) \to \sH^{\pol}(\sD_0)
%%   \]
%%   is an isomorphism of $\ho(\Ch)$.
%% \end{corollary}
%% We can now prove the main result of this section.

\begin{proposition}\label{prop:contractibleisgood}
  Every oplax contractible $\oo$\nbd{}category $C$ is \good{} and we have
  \[
  \sH^{\pol}(C)\simeq \sH^{\sing}(C)\simeq \mathbb{Z}
  \]
  where $\mathbb{Z}$ is seen as an object of $\ho(\Ch)$ concentrated in degree $0$.
\end{proposition}
\begin{proof}
Consider the commutative square
  \[
  \begin{tikzcd}
    \sH^{\pol}(C) \ar[d,"\sH^{\pol}(p_C)"] \ar[r,"\pi_C"] & \sH^{\sing}(C) \ar[d,"\sH^{\sing}(p_C)"] \\
    \sH^{\pol}(\sD_0) \ar[r,"\pi_{\sD_0}"] & \sH^{\sing}(\sD_0).
  \end{tikzcd}
  \]
  It follows respectively from Proposition \ref{prop:oplaxhmtpyisthom} and
  Proposition \ref{prop:oplaxhmtpypolhmlgy} that the right and left vertical
  morphisms of the above square are isomorphisms. Then, an immediate computation
  left to the reader shows that $\sD_0$ is \good{} and that
  $\sH^{\pol}(\sD_0)\simeq \sH^{\sing}(\sD_0)\simeq \mathbb{Z}$. By a 2-out-of-3
  property, we deduce that $\pi_C : \sH^{\sing}(C)\to \sH^{\pol}(C)$ is an
  isomorphism and $\sH^{\pol}(C)\simeq \sH^{\sing}(C)\simeq \mathbb{Z}$.
\end{proof}
\begin{remark}
  Definition \ref{def:contractible} admits an obvious ``lax'' variation and Proposition \ref{prop:contractibleisgood} is also true for lax contractible $\oo$\nbd{}categories.
  \end{remark}
We end this section with an important result on slice $\oo$\nbd{}categories (Paragraph \ref{paragr:slices}).
\begin{proposition}\label{prop:slicecontractible}
  Let $A$ be an $\oo$\nbd{}category and $a_0$ an object of $A$. The $\oo$\nbd{}category $A/a_0$ is oplax contractible. 
   \end{proposition}
\begin{proof}
  This follows from the dual of \cite[Proposition 5.22]{ara2020theoreme}.
  \end{proof}
\section{Homology of globes and spheres}
\begin{lemma}\label{lemma:globescontractible}
  For every $n \in \mathbb{N}$, the $\oo$\nbd{}category $\sD_n$ is oplax contractible.
\end{lemma}
\begin{proof}
  Recall that we write $e_n$ for the unique non-trivial $n$\nbd{}cell of $\sD_n$ and that by definition $\sD_n$ has exactly two non-trivial $k$\nbd{}cells for every $k$ such that $0\leq k<n$. These two $k$\nbd{}cells are parallel and are given by $\src_k(e_n)$ and $\trgt_k(e_n)$.

  Let $r : \sD_0 \to \sD_n$ be the $\oo$\nbd{}functor that points to $\trgt_0(e_n)$ (which means that $r=\langle \trgt_0(e_n) \rangle$ with the notations of \ref{paragr:defglobe}). For every $k$\nbd{}cell $x$ of $\sD_n$, we have
  \[
  r(p(x))=\1^k_{\trgt_0(e_n)},
  \]
  where we write $p$ for the unique $\oo$\nbd{}functor $\sD_n \to \sD_0$.

  Now for $0 \leq k <n$, we define $\alpha_{\src_k(e_n)}$ and $\alpha_{\trgt_k(e_n)}$ as
  \[
  \alpha_{\src_k(e_n)}=\begin{cases}\trgt_{k+1}(e_n), \text{ if } k<n-1 \\ e_n, \text{ if } k=n-1\end{cases} \text{ and }  \alpha_{\trgt_k(e_n)}=\begin{cases}1_{\trgt_k(e_n)}, \text{ if } k<n-1 \\ e_n, \text{ if } k=n-1.\end{cases}
  \]
  It is straightforward to check that this data defines an oplax transformation \[\alpha : \mathrm{id}_{\sD_n} \Rightarrow r\circ p\] (see \ref{paragr:formulasoplax} and Example \ref{example:natisoplax}), which proves the result. 
  %% \[
  %% \alpha_{\src_k(e_n)}=\trgt_{k+1}(e_n) \text{ and } \alpha_{\trgt_k(e_n)}=1_{\trgt_k(e_n)},
  %% \]
  %% and also
  %% \[
  %% \alpha_{\src_{n-1}(e_n)}=e_n=\alpha_{\src_{n-1}(e_n)}.
  %% \]
\end{proof}
In particular, for every $n \in \mathbb{N}$, $\sD_n$ is \good{}. Recall from \ref{paragr:inclusionsphereglobe} that for every $n \geq 0$, we have a cocartesian square
\[
\begin{tikzcd}
     \sS_{n-1} \ar[r,"i_n"] \ar[d,"i_n"] & \sD_n \ar[d,"j_n^+"]\\
    \sD_n \ar[r,"j_n^-"'] & \sS_{n}.
    \ar[from=1-1,to=2-2,phantom,very near end,"\ulcorner"]
  \end{tikzcd}
\]
\begin{lemma}\label{lemma:squarenerve}
  For every $n \geq 0$, the commutative square of simplicial sets
  \[
  \begin{tikzcd}
    N_{\oo}(\sS_{n-1}) \ar[r,"N_{\oo}(i_n)"] \ar[d,"N_{\oo}(i_n)"] & N_{\oo}(\sD_{n}) \ar[d,"N_{\oo}(j_n^+)"] \\
    N_{\oo}(\sD_{n}) \ar[r,"N_{\oo}(j_n^-)"] & N_{\oo}(\sS_{n})
  \end{tikzcd}
  \]
  is cocartesian.
\end{lemma}
\begin{proof}
  Since colimits in presheaf categories are computed pointwise, what we need
  to show is that for every $k\geq 0$, the following commutative square is
  cocartesian
  \begin{equation}\label{squarenervesphere}
    \begin{tikzcd}[column sep=huge,row sep=huge]
    \Hom_{\oo\Cat}(\Or_k,\sS_{n-1}) \ar[r,"{\Hom_{\oo\Cat}(\Or_k,i_n)}"] \ar[d,"{\Hom_{\oo\Cat}(\Or_k,i_n)}"'] & \Hom_{\oo\Cat}(\Or_k,\sD_{n}) \ar[d,"{\Hom_{\oo\Cat}(\Or_k,j_n^+)}"] \\
    \Hom_{\oo\Cat}(\Or_k,\sD_{n}) \ar[r,"{\Hom_{\oo\Cat}(\Or_k,j_n^-)}"'] & \Hom_{\oo\Cat}(\Or_k,\sS_{n}).
    \end{tikzcd}
  \end{equation}
  Notice first that the square
  \[
    \begin{tikzcd}
      \sS_{n-1} \ar[r,"i_n"] \ar[d,"i_n"] & \sD_n \ar[d,"j_n^+"]\\
      \sD_n \ar[r,"j_n^-"'] & \sS_{n}.
    \end{tikzcd}
  \]
  is cartesian and all four morphisms are monomorphisms. Since the
  functor \[\Hom_{\oo\Cat}(\Or_k,-):\oo\Cat \to \Set \] preserves limits, the square
  \eqref{squarenervesphere} is a cartesian square of $\Set$ all of
  whose four morphisms are monomorphisms.  Hence, in order to prove
  that square \eqref{squarenervesphere} is cocartesian, we only need
  to show that for every $k \geq 0$ and every $\oo$\nbd{}functor
  $\varphi : \Or_k \to \sS_{n}$, there exists an $\oo$\nbd{}functor
  $\varphi' : \Or_k \to \sD_n$ such that either $j_n^+ \circ \varphi '
  = \varphi$ or $j_n^- \circ \varphi' = \varphi$.
  
  %% Notice now that the morphisms $j_n^+$ and $j_n^-$ trivially satisfy the following
  %% properties:
  %% \begin{enumerate}[label=(\roman*)]
  %% \item Every $k$\nbd{}cell of $\sS_n$ with $0 \leq k <n$ is the image by
  %%   $j^+_n$ of a (unique) $k$\nbd{}cell of $\sD_n$ \emph{and} the image by
  %%   $j^-_n$ of a (unique) $k$\nbd{}cell of $\sD_n$
  %% \item Every non-trivial $n$\nbd{}cell of $\sS_n$ (there are only two of them) is the image either by 
  %%   $j^+_n$ or by $j^-_n$ of a non-trivial $n$\nbd{}cell of $\sD_n$.
  %% \end{enumerate}
  For convenience, let us write $h_n^+$ (resp.\ $h_n^-$) for the only generating $n$\nbd{}cell of $\sS_n$ contained in the image of $j^+_n$ (resp.\ $j_n^-$). The cells $h_n^+$ and $h_n^-$ are the only non-trivial $n$\nbd{}cells of $\sS_n$. We also write $\alpha_k$ for the principal cell of $\Or_k$ (see \ref{paragr:orientals}). This is the only non-trivial $k$\nbd{}cell of $\Or_k$.

  Now, let $\varphi : \Or_k \to \sS_n$ be an $\oo$\nbd{}functor. There are several cases to distinguish.
  \begin{description}
  \item[Case $k<n$:] Since every generating cell of $\gamma$ of $\Or_k$ is of
    dimension not greater than $k$, the cell $\varphi(\gamma)$ is of dimension strictly lower than $n$. Since all cells of dimension strictly lower than $n$ are both in the image of $j^+_n$ and in the image of $j^-_n$, $\varphi$ obviously factors through $j^+_n$ (and $j^-_n$).
  \item[Case $k=n$:] The image of $\alpha_n$ is either a non-trivial $n$\nbd{}cell of $\sS_n$ or a unit on a strictly lower dimensional cell. In the second situation, everything works like the case $k<n$. Now suppose for example that $\varphi(\alpha_n)$ is $h^+_n$, which is in the image of $j^+_n$. Since all of the other generating cells of $\Or_n$ are of dimension strictly lower than $n$, their images by $\varphi$ are also of dimension strictly lower than $n$ and hence, are all contained in the image of $j^+_n$. Altogether this proves that $\varphi$ factors through $j^+_n$. The case where $\varphi(\alpha_n)=h^-_n$ is symmetric.
  \item[Case $k>n$:] Since $\sS_n$ is an $n$\nbd{}category, the image of
    $\alpha_k$ is necessarily of the form $\varphi(\alpha_k)=\1^k_{x}$ with $x$
    a cell of $\sS_n$ of dimension non-greater than $n$. If $x$ is a unit on a
    cell whose dimension is strictly lower than $n$, then everything works like
    in the case $k<n$. If not, this means that $x$ is a non-trivial
    $n$\nbd{}cell of $\sS_n$. Suppose for example that $x=h^+_n$. Now let
    $\gamma$ be a generator of $\Or_k$ of dimension $k-1$. We have
    $\varphi(\gamma)=\1^{k-1}_y$ with $y$ which is either a unit on a cell of
    dimension strictly lower than $n$, or a non-degenerate $n$\nbd{}cell of
    $\sS_n$ (if $k-1=n$, we use the convention that $\1^{k-1}_y=y$). In the
    first situation, $y$ is in the image of $j^+_n$ as in the case $k<n$, and
    thus, so is $\1^{k-1}_y$. In the second situation, this means \emph{a
      priori} that either $y=h_n^+$ or $y=h_n^-$. But we know that $\gamma$ is
    part of a composition that is equal to either the source or the target of
    $\alpha_k$ (see \ref{paragr:orientals}) and thus, $y$ is part of a composition that is equal to either the source or the target of $x=h^+_n$. Since no composition involving $h^-_n$ can be equal to $h^+_n$ (one could invoke the function introduced in \ref{prop:countingfunction}), this implies that $y=h_n^+$ and hence, $f(\gamma)$ is in the image of $j^+_n$. This goes for all generating cells of dimension $k-1$ of $\Or_k$ and we can recursively apply the same reasoning for generating cells of dimension $k-2$, then $k-3$ and so forth. Altogether, this proves that $\varphi$ factorizes through $j^+_n$. The case where $x=h^-_n$ and $\varphi$ factorizes through $j^-_n$ is symmetric.
    \end{description}
\end{proof}
From these two lemmas, follows the important proposition below.
\begin{proposition}\label{prop:spheresaregood}
  For every $n \geq -1$, the $\oo$\nbd{}category $\sS_n$ is \good{}.
\end{proposition}
\begin{proof}
  %% We proceed by induction on $n$. When $n=-1$, it is trivial to check that the
  %% empty $\oo$\nbd{}category is \good{}.
  Recall that the cofibrations of simplicial sets are exactly the monomorphisms, and in particular all simplicial sets are cofibrant. Since $i_n : \sS_{n-1} \to \sD_n$ is a monomorphism for every $n \geq 0$ and since $N_{\oo}$ preserves monomorphisms (as a right adjoint), it follows from Lemma \ref{lemma:squarenerve} and Lemma \ref{lemma:hmtpycocartesianreedy} that the square
\[
  \begin{tikzcd}
    N_{\oo}(\sS_{n-1}) \ar[r,"N_{\oo}(i_n)"] \ar[d,"N_{\oo}(i_n)"] & N_{\oo}(\sD_{n}) \ar[d,"N_{\oo}(j_n^+)"] \\
    N_{\oo}(\sD_{n}) \ar[r,"N_{\oo}(j_n^-)"] & N_{\oo}(\sS_{n})
  \end{tikzcd}
  \]
is a homotopy cocartesian square of simplicial sets. Since $N_{\oo}$ induces an
  equivalence of op-prederivators $\Ho(\oo\Cat^{\Th}) \to \Ho(\Psh{\Delta})$
  (Theorem \ref{thm:gagna}), it follows that the square of $\oo\Cat$
  \[
    \begin{tikzcd}
    \sS_{n-1} \ar[r,"i_n"] \ar[d,"i_n"] & \sD_{n} \ar[d,"j_n^+"] \\
    \sD_{n} \ar[r,"j_n^-"] & \sS_{n}
    \ar[from=1-1,to=2-2,phantom,"\ulcorner",very near end]
    \end{tikzcd}
    \]
  is Thomason homotopy cocartesian for every $n\geq 0$. Finally, since $i_n : \sS_{n-1} \to \sD_{n}$ is
  a folk cofibration and $\sS_{n-1}$ and $\sD_{n}$ are folk cofibrant for every $n\geq0$, we deduce the desired result from Corollary \ref{cor:usefulcriterion} and an immediate induction. The base case being simply that $\sS_{-1}=\emptyset$ is obviously \good{}.
\end{proof}
\begin{paragr}
  The previous proposition implies what we claimed in Paragraph \ref{paragr:prelimcriteriongoodcat}, which is that the morphism of op-prederivators
  \[
  \J : \Ho(\oo\Cat^{\folk}) \to \Ho(\oo\Cat^{\Th})
  \]
  induced by the identity functor of $\oo\Cat$ is \emph{not} homotopy cocontinuous.
  Indeed, recall from Paragraph \ref{paragr:bubble} that the category
  $B^2\mathbb{N}$ is \emph{not} \good{}; but on the other hand we have a
  cocartesian square
  \[
  \begin{tikzcd}
    \sS_1 \ar[r] \ar[d,"i_2"] & \sD_0 \ar[d] \\
    \sD_2 \ar[r] & B^2\mathbb{N},
    \ar[from=1-1,to=2-2,phantom,"\ulcorner",very near end]
  \end{tikzcd}
  \]
  where the map $\sD_2 \to B^2\mathbb{N}$ points the unique generating
  $2$-cell of $B^2\mathbb{N}$ and $\sD_0 \to B^2\mathbb{N}$ points to
  the only object of $B^2\mathbb{N}$. Since $\sS_1$, $\sD_0$ and $\sD_2$ are
  free and $i_2$ is a folk cofibration,
  the square is also folk homotopy cocartesian. If $\J$ was homotopy
  cocontinuous, then this square would also be Thomason homotopy
  cocartesian. Since we know that $\sS_1$, $\sD_0$ and $\sD_2$ are
  \good{}, this would imply that $B^2\mathbb{N}$ is \good{}.

  From Proposition \ref{prop:spheresaregood}, we also deduce the proposition
  below which gives a criterion to detect \good{} $\oo$\nbd{}categories when we
  already know that they are free. 
\end{paragr}
\begin{proposition}
  Let $C$ be a free $\oo$\nbd{}category and for every $k \in \mathbb{N}$ let
  $\Sigma_k$ be its $k$\nbd{}basis. If for every $k \in \mathbb{N}$, the following
  cocartesian square (see \ref{def:nbasis})
  \[
  \begin{tikzcd}[column sep=large]
   \displaystyle \coprod_{x \in \Sigma_k} \sS_{k-1} \ar[d,"\coprod i_k"'] \ar[r,"{\langle s(x),t(x) \rangle}"]& \sk_{k-1}(C) \ar[d,hook] \\
    \displaystyle \coprod_{x \in \Sigma_k} \sD_{k} \ar[r,"\langle x \rangle"] &
    \sk_k(C)
    \ar[from=1-1,to=2-2,phantom,"\ulcorner",very near end]
  \end{tikzcd}
  \]
  is Thomason homotopy cocartesian, then $C$ is \good{}.
\end{proposition}
\begin{proof}
  Since the morphisms $i_k$ are folk cofibrations and the
  $\oo$\nbd{}categories $\sS_{k-1}$ and $\sD_{k}$ are folk cofibrant
  and \good{}, it follows from Corollary \ref{cor:usefulcriterion} and
  an immediate induction that all $\sk_k(C)$ are \good{}. The result
  follows then from Lemma \ref{lemma:filtration}, Corollary \ref{cor:cofprojms} and Proposition
  \ref{prop:sequentialhmtpycolimit}.
\end{proof}
\section{The miraculous case of 1-categories}
Recall that the terms \emph{1-category} and \emph{(small) category} are
synonymous. While we have used the latter one more often so far, in this section
we will mostly use the former one. As usual, the canonical functor $\iota_1 :
\Cat \to \oo\Cat$ is treated as an inclusion functor and hence we always consider
$1$\nbd{}categories as particular cases of $\oo$\nbd{}categories.

The goal of what follows is to show that every $1$\nbd{}category is \good{}. In order to do that, we will prove that every 1-category
is a canonical colimit of contractible $1$\nbd{}categories and that this colimit is
homotopic both
with respect to the folk weak equivalences and with respect to the Thomason equivalences.
We call the reader's attention to an important subtlety here: even though the
desired result only refers to $1$\nbd{}categories, we have to work in the setting
of $\oo$\nbd{}categories. This can be explained from the fact that if we take a
cofibrant resolution of a $1$\nbd{}category $C$ in the folk model structure on $\oo\Cat$
\[
P \to C,
\]
then $P$ is not necessarily a $1$\nbd{}category. In particular, polygraphic
homology groups of a $1$\nbd{}category need \emph{not} be trivial in dimension
higher than $1$.
\begin{paragr}
 Let $A$ be a $1$\nbd{}category and $a$ an object of $A$. Recall that we write $A/a$ for the slice $1$\nbd{}category of $A$ over $a$, that is the $1$\nbd{}category whose description is as follows:
  \begin{itemize}[label=-]
  \item an object of $A/a$ is a pair $(a', p : a' \to a)$ where $a'$ is an object of $A$ and $p$ is an arrow of $A$,
  \item an arrow of $A/a$ is a pair $(q,p : a' \to a)$ where $p$ is an arrow of
    $A$ and $q$ is an arrow of $A$ of the form $q : a'' \to a'$. The target of
    $(q,p)$ is given by $(a',p)$ and the source by $(a'',p\circ q)$. % $ q : a' \to a''$ of $A$ such that $p'\circ q = p$.
  \end{itemize}
  We write $\pi_a$ for the canonical forgetful functor
   \[
  \begin{aligned}
    \pi_{a} : A/a &\to A \\
    (a',p) &\mapsto a'.
  \end{aligned}
  \]
  This is a special case of the more general notion of slice $\oo$\nbd{}category introduced in \ref{paragr:slices}. In particular, given an $\oo$\nbd{}category $X$ and an $\oo$\nbd{}functor $f : X \to A$, we have defined the $\oo$\nbd{}category $X/a$ and the $\oo$\nbd{}functor
  \[
  f/a : X/a \to A/a
  \]
  as the following pullback
  \[
  \begin{tikzcd}
    X/a \ar[r]\ar[dr, phantom, "\lrcorner", very near start] \ar[d,"f/a"'] & X \ar[d,"f"] \\
    A/a \ar[r,"\pi_{a}"] &A.
  \end{tikzcd}
  \]
  More explicitly, the $n$\nbd{}cells of $X/a$ can be described as pairs $(x,p)$
  where $x$ is an $n$\nbd{}cell of $X$ and $p$ is an arrow of $A$ of the form
  \[
    p : f(x)\to a \text{ if }n=0
  \]
  and
  \[
    p : f(\trgt_0(x)) \to a \text{ if }n>0.
  \]
  \emph{From now on, let us use the convention that $\trgt_0(x)=x$ when $x$ is a
    $0$\nbd{}cell of $X$}.

  When $n>0$, the source and target of an $n$\nbd{}cell $(x,p)$ of $X/a$ are given by
  \[
  \src((x,p))=(\src(x),p) \text{ and } \trgt((x,p))=(\trgt(x),p).
\]
%% LA DESCRIPTION DU BUT AU DESSUS N'EST PAS BONNE POUR LA DIMENSION 1
%% A CORRIGER !!
  Moreover, the $\oo$\nbd{}functor $f/a$ is described as 
  \[
  (x,p) \mapsto (f(x),p),
  \]
  and the canonical $\oo$\nbd{}functor $X/a \to X$ as
  \[
  (x,p) \mapsto x.
  \]
\end{paragr}
\begin{paragr}\label{paragr:unfolding}
  Let $f : X \to A$ be an $\oo$\nbd{}functor with $A$ a $1$\nbd{}category. Every arrow $\beta : a \to a'$ of $A$ induces an $\oo$\nbd{}functor
  \begin{align*}
    X/\beta : X/a &\to X/{a'} \\
    (x,p) & \mapsto (x,\beta \circ p),
  \end{align*}
  which takes part in a commutative triangle
  \[
  \begin{tikzcd}[column sep=tiny]
    X/{a} \ar[rr,"X/{\beta}"] \ar[dr] && X/{a'} \ar[dl] \\
    &X&.
  \end{tikzcd}
  \]
  This defines a functor
  \begin{align*}
    X/{-} : A &\to \oo\Cat\\
    a &\mapsto X/a
  \end{align*}
  and a canonical $\oo$\nbd{}functor
  \[
  \colim_{a \in A} (X/{a}) \to X.
  \]
  Let $f' : X' \to A$ be another $\oo$\nbd{}functor and let
  \[
  \begin{tikzcd}
    X \ar[rr,"g"] \ar[dr,"f"'] && X' \ar[dl,"f'"] \\
    &A&
  \end{tikzcd}
  \]
  be a commutative triangle in $\oo\Cat$. Recall from \ref{paragr:slices} that for every object $a$ of $A$, there is an $\oo$\nbd{}functor $g/a$ defined as
  \begin{align*}
    g/a : X/a &\to X'/a \\
    (x,p) &\mapsto (g(x),p).
  \end{align*}
  This defines a natural transformation
  \[g/- : X/- \Rightarrow X'/-,\]
  and thus induces an $\oo$\nbd{}functor
  \[
  \colim_{a \in A}(X/a) \to \colim_{a \in A}(X'/a).
  \]
  Furthermore, it is immediate to check that the square
  \[
  \begin{tikzcd}
  \displaystyle \colim_{a \in A}(X/a) \ar[d] \ar[r] & X \ar[d,"g"] \\
  \displaystyle\colim_{a \in A}(X'/a) \ar[r] & X',
  \end{tikzcd}
  \]
  is commutative.
\end{paragr}
\begin{lemma}\label{lemma:colimslice}
  Let $f : X \to A$ be an $\oo$\nbd{}functor such that $A$ is a $1$\nbd{}category. The canonical $\oo$\nbd{}functor
  \[
  \colim_{a \in A}(X/a) \to X
  \]
  is an isomorphism.
\end{lemma}
\begin{proof}
 We have to show that the cocone
 \[
   (X/a \to X)_{a \in \Ob(A)}
 \]
 is colimiting.
 Let
 \[
   (\phi_{a} : X/a \to C)_{a \in \Ob(A)}
 \]
 be another cocone and let $x$ be a $n$\nbd{}arrow of $X$. Notice that the pair
 \[
   (x,1_{f(\trgt_0(x))})
 \]
 is a $n$\nbd{}arrow of $X/f(\trgt_0(x))$. We leave it to the reader to check that the formula
\begin{align*}
     \phi : X &\to C \\
     x &\mapsto \phi_{f(\trgt_0(x))}(x,1_{f(\trgt_0(x))}).
   \end{align*}
 defines an $\oo$\nbd{}functor and it is straightforward to check that for every object $a$ of $A$ the triangle
 \[
   \begin{tikzcd}
     X/a\ar[dr,"\phi_{a}"']\ar[r] & X \ar[d,"\phi"] \\
     & C
   \end{tikzcd}
 \]
 is commutative. This proves the existence part of the universal property.
 
  Now let $\phi' : X \to C$ be another $\oo$\nbd{}functor that makes the previous triangle commute for every object $a$ of $A$ and let $x$ be an $n$\nbd{}cell of $X$. Since the triangle
 \[
   \begin{tikzcd}
     X/f(\trgt_0(x)) \ar[dr,"\phi_{f(\trgt_0(x))}"']\ar[r] & X \ar[d,"\phi'"] \\
     & C
   \end{tikzcd}
 \]
 is commutative, we necessarily have
 \[
   \phi'(x)=\phi_{f(\trgt_0(x))}(x,1_{f(\trgt_0(x))})
 \]
 which proves that $\phi'=\phi$.
\end{proof}
\begin{paragr}
  In particular, when we apply the previous lemma to $\mathrm{id}_A : A \to A$,
  we obtain that every $1$\nbd{}category $A$ is (canonically isomorphic to) the colimit
  \[
  \colim_{a \in A} (A/a).
\]
% In other words, this simply say that the colimit of the Yoneda embedding $A \to
% \Psh{A}$ is the terminal presheaves
  We now proceed to prove that this colimit is homotopic with respect to
  the folk weak equivalences.
\end{paragr}
Up to Lemma \ref{lemma:basisofslice}, we fix once and for all an $\oo$\nbd{}functor $f : X \to A$ with $A$ a $1$\nbd{}category.
\begin{lemma}\label{lemma:sliceisfree}
  If $X$ is free, then for every object $a$ of $A$, the $\oo$\nbd{}category
  $X/a$ is free. More precisely, if $\Sigma^X_n$ is the $n$\nbd{}basis of $X$,
  then the $n$\nbd{}basis of $X/a$ is the set
  \[
  \Sigma^{X/a}_n := \{(x,p) \in (X/a)_n \vert x \in \Sigma^X_n \}.
  \]
\end{lemma}
\begin{proof}
  It is immediate to check that for every object $a$ of $A$, the canonical
  forgetful functor $\pi_{a} : A/a \to A$ is a discrete Conduché functor (see Section
  \ref{section:conduche}). Hence, from Lemma \ref{lemma:pullbackconduche} we
  know that $X/a \to X$ is a discrete Conduché $\oo$\nbd{}functor. The result follows
  then from Theorem \ref{thm:conduche}.
\end{proof}
\begin{paragr}
  When $X$ is free, every arrow $\beta : a \to a'$ of $A$ induces a map
  \begin{align*}
    \Sigma^{X/a}_n &\to \Sigma^{X/a'}_n \\
    (x,p) &\mapsto (x,\beta\circ p).
  \end{align*}
  This defines a functor
  \begin{align*}
    \Sigma^{X/{\shortminus}}_n : A &\to \Set \\
    a &\mapsto \Sigma^{X/a}_n.
  \end{align*}
\end{paragr}
\begin{lemma}\label{lemma:basisofslice}
  If $X$ is free, then there is an isomorphism of functors 
  \[
  \Sigma^{X/\shortminus}_n \simeq \coprod_{x \in \Sigma^X_n}\Hom_A\left(f(\trgt_0(x)),\shortminus\right).
  \]
\end{lemma}
\begin{proof}
  For every object $a$ of $A$ and every $x \in \Sigma_n^X$, we have a canonical map
  \begin{align*}
    \Hom_A\left(f(\trgt_0(x)),a\right) &\to \Sigma^{X/a}_n \\
    p &\mapsto (x,p).
  \end{align*}
  By universal property, this induces a map
  \[
  \coprod_{x \in \Sigma^X_n}\Hom_A\left(f(\trgt_0(x)),a\right) \to  \Sigma^{X/a}_n,
  \]
  which is natural in $a$. A simple verification shows that it is a bijection.
\end{proof}
\begin{proposition}\label{prop:sliceiscofibrant}
  Let $A$ be a $1$\nbd{}category, $X$ be a free $\oo$\nbd{}category and $f : X \to A$ be an $\oo$\nbd{}functor. The functor
  \begin{align*}
    A &\to \oo\Cat \\
    a &\mapsto X/a
  \end{align*}
  is a cofibrant object for the projective model structure on $\oo\Cat(A)$
  induced by the folk model structure on $\oo\Cat$ (\ref{paragr:projmod}).
\end{proposition}
\begin{proof}
  Recall that the set
  \[
    \{i_n: \sS_{n-1} \to \sD_n \vert n \in \mathbb{N} \}
  \]
  is a set of generating folk cofibrations.
  From Lemmas \ref{lemma:sliceisfree} and \ref{lemma:basisofslice} we deduce
  that for every object $a$ of $A$ and every $n \in \mathbb{N}$, the canonical square
  \[
    \begin{tikzcd}
      \displaystyle\coprod_{x \in \Sigma^X_n}\coprod_{\Hom_A(f(\trgt_0(x)),a)}\sS_{n-1} \ar[r] \ar[d] & \sk_{n-1}(X/a) \ar[d]\\
      \displaystyle\coprod_{x \in \Sigma^X_n}\coprod_{\Hom_A(f(\trgt_0(x)),a)}\sD_n \ar[r]& \sk_n{(X/a)}
    \end{tikzcd}
  \]
  is cocartesian. It is straightforward to check that this square is natural in
  $a$ in an obvious sense, which means that we have a cocartesian square in $\oo\Cat(A)$:
  \[
      \begin{tikzcd}
      \displaystyle\coprod_{x \in \Sigma^X_n}\sS_{n-1}\otimes f(\trgt_0(x)) \ar[r] \ar[d] & \sk_{n-1}(X/-) \ar[d]\\
      \displaystyle\coprod_{x \in \Sigma^X_n}\sD_n\otimes f(\trgt_0(x)) \ar[r]& \sk_n{(X/-)}
      \ar[from=1-1,to=2-2,phantom,"\ulcorner",very near end]
    \end{tikzcd}
  \]
  (see \ref{paragr:cofprojms} for notations). From the second part of Proposition \ref{prop:modprs}, we deduce that for every $n\geq 0$, \[\sk_{n-1}(X/-) \to \sk_{n}(X/-)\] is a cofibration for the
  projective model structure on $\oo\Cat(A)$. Thus, the transfinite composition
  \[
  \emptyset \to \sk_{0}(X/-) \to \sk_{1}(X/) \to \cdots \to \sk_{n}(X/-) \to \cdots,
  \]
  which is canonically isomorphic to $\emptyset \to X/-$ (see Lemma \ref{lemma:filtration}), is also a cofibration for the projective model structure. 
\end{proof}
\begin{corollary}\label{cor:folkhmtpycol}
  Let $A$ be a $1$\nbd{}category and $f : X \to A$ be an $\oo$\nbd{}functor. The canonical arrow of $\ho(\oo\Cat^{\folk})$
  \[
  \hocolim^{\folk}_{a \in A}(X/a) \to X,
  \]
  induced by the co-cone $(X/a \to X)_{a \in \Ob(A)}$, is an isomorphism.
\end{corollary}
Beware that in the previous corollary, we did \emph{not} suppose that $X$ was free.
\begin{proof}
  Let $P$ be a free $\omega$-category and $g : P \to X$ a folk trivial fibration
  and consider the following commutative diagram of $\ho(\oo\Cat^{\folk})$ 
  \begin{equation}\label{comsquare}
    \begin{tikzcd}
      \displaystyle\hocolim^{\folk}_{a \in A}(P/a) \ar[d] \ar[r] & \displaystyle\colim_{a \in A}(P/a) \ar[d] \ar[r] & P \ar[d]\\
      \displaystyle\hocolim^{\folk}_{a \in A}(X/a)  \ar[r] & \displaystyle\colim_{a \in A}(X/a) \ar[r] & X
    \end{tikzcd}
  \end{equation}
  where the middle and most left vertical arrows are induced by the arrows
  \[
    g/a : P/a \to X/a,
  \]
  and the most right vertical arrow is induced by $g$.
  Since trivial fibrations are stable by pullback, $g/a$ is a trivial fibration.
  This proves that the most left vertical arrow of diagram \eqref{comsquare} is an isomorphism.
  
  Now, from Proposition \ref{prop:sliceiscofibrant} and Corollary
  \ref{cor:cofprojms}, we deduce that the arrow \[\hocolim_{a \in
    A}^{\folk}(P/a)\to \colim_{a \in A}(P/a)\] is an isomorphism. Moreover, from Lemma \ref{lemma:colimslice}, we know that the arrows
  \[\colim_{a \in A}(P/a)\to P\] and \[\colim_{a \in A}(X/a)\to X\] are
  isomorphisms. 

  Finally, since $g$ is a folk weak equivalence, the most right vertical arrow of diagram \eqref{comsquare} is an
  isomorphism and by an immediate 2-out-of-3 property this proves
  that all arrows of \eqref{comsquare} are isomorphisms. In particular, so is
  the composition of the two bottom horizontal arrows, which is what we desired
  to show.
\end{proof}
We now move on to the next step needed to prove that every $1$\nbd{}category is \good{}. For that purpose, let us recall a construction commonly referred to as the ``Grothendieck construction''.
\begin{paragr}
  Let $A$ be a $1$\nbd{}category and $F : A \to \Cat$ a functor. We denote by $\int F$ or $\int_{a \in A}F(a)$ the category such that:
  \begin{itemize}[label=-]
  \item An object of $\int F$ is a pair $(a,x)$ where $a$ is an object of $A$ and $x$ is an object of $F(a)$.
  \item An arrow $(a,x) \to (a',x')$ of $\int F$ is a pair $(f,k)$ where
    \[
    f : a \to a'
    \]
    is an arrow of $A$, and
    \[
    k : F(f)(x) \to x'.
    \]
  \end{itemize}
  The unit on $(a,x)$ is the pair $(1_a,1_x)$ and the composition of $(f,k) : (a,x) \to (a',x')$ and $(f',k') : (a',x') \to (a'',x'')$ is given by:
  \[
  (f',k')\circ(f,k)=(f'\circ f,k'\circ F(f')(k)).
  \]
  Every natural transformation
  \[
  \begin{tikzcd}
    A \ar[r,bend left,"F",""{name=A,below},pos=19/30]\ar[r,bend right,"G"',""{name=B, above},pos=11/20] & \Cat \ar[from=A,to=B,Rightarrow,"\alpha",pos=9/20]
  \end{tikzcd}
  \]
  induces a functor
  \begin{align*}
    \int \alpha : \int F &\to \int G\\
    (a,x) &\mapsto (a,\alpha_a(x)).
  \end{align*}
  Altogether, this defines a functor
  \begin{align*}
    \int : \Cat(A)&\to \Cat \\
   F&\mapsto \int F,
  \end{align*}
  where $\Cat(A)$ is the category of functors from $A$ to $\Cat$. 
\end{paragr}
We now recall an important theorem due to Thomason.
\begin{theorem}[Thomason]\label{thm:Thomason}
  The functor $\int : \Cat(A) \to \Cat$ sends the pointwise Thomason equivalences (\ref{paragr:homder}) to Thomason equivalences and the induced functor
  \[
  \overline{\int} : \ho(\Cat^{\Th}(A)) \to \ho(\Cat^{\Th})
  \]
  is canonically isomorphic to the homotopy colimit functor
  \[
  \hocolim^{\Th}_A : \ho(\Cat^{\Th}(A)) \to \ho(\Cat^{\Th}).
  \]
\end{theorem}
\begin{proof}
  The original source for this Theorem is \cite{thomason1979homotopy}. However, the definition of homotopy colimit used by Thomason, albeit equivalent, is not the same as the one we used in this dissertation and is slightly outdated. A more modern proof of the theorem can be found in \cite[Proposition 2.3.1 and Théorème 1.3.7]{maltsiniotis2005theorie}.
\end{proof}
\begin{corollary}\label{cor:thomhmtpycol}
  Let $A$ be a $1$\nbd{}category. The canonical map
  \[
  \hocolim^{\Th}_{a \in A}(A/a) \to A
  \]
  induced by the co-cone $(A/a \to A)_{a \in \Ob(A)}$, is an isomorphism of $\ho(\Cat^{\Th})$. 
\end{corollary}
\begin{proof}
  For every object $a$ of $A$, the canonical map to the terminal category
  \[
  A/a \to \sD_0
  \]
  is a Thomason equivalence. This comes from the fact that $A/a$ is oplax contractible (Proposition \ref{prop:slicecontractible}), or from \cite[Section 1, Corollary 2]{quillen1973higher} and the fact that $A/a$ has a terminal object.

  In particular, the morphism of functors
  \[
  A/(-) \Rightarrow k_{\sD_0},
  \]
  where $k_{\sD_0}$ is the constant functor $A \to \Cat$ with value the terminal category $\sD_0$, is a pointwise Thomason equivalence. It follows from the first part of Theorem \ref{thm:Thomason} that
  \[
  \int_{a \in A}A/a \to \int_{a \in A}k_{\sD_0}
  \]
  is a Thomason equivalence and an immediate computation shows that \[\int_{a \in A}k_{\sD_0} \simeq A.\] From the second part of Theorem \ref{thm:Thomason}, we have that
  \[
  \hocolim^{\Th}_{a \in A}(A/a) \simeq A.
  \]
  A thorough analysis of all the isomorphisms involved shows that this last isomorphism is indeed induced by the co-cone $(A/a \to A)_{a \in \Ob(A)}$.
\end{proof}
\begin{remark}
  It is possible to extend the previous corollary to prove that for every functor $f : X \to A$ ($X$ and $A$ being $1$\nbd{}categories), we have \[\hocolim^{\Th}_{a \in A} (X/a) \simeq X.\] However, to prove that it is also the case when $X$ is an $\oo$\nbd{}category and $f$ an $\oo$\nbd{}functor, as in Corollary \ref{cor:folkhmtpycol}, one would need to extend the Grothendieck construction to functors with value in $\oo\Cat$ and to prove an $\oo$\nbd{}categorical analogue of Theorem \ref{thm:Thomason}. Such results, while being highly plausible, go beyond the scope of this dissertation.
\end{remark}
Putting all the pieces together, we are now able to prove the awaited theorem.
\begin{theorem}\label{thm:categoriesaregood}
  Every $1$\nbd{}category is \good{}.
\end{theorem}
\begin{proof}
  All the arguments of the proof have already been given and we sum them up here essentially for the sake of clarity.
  Let $A$ be a $1$\nbd{}category. Consider the diagram
  \begin{align*}
    A &\to \oo\Cat\\
    a &\mapsto A/a
  \end{align*}
  and the co-cone
  \[
  (A/a \to A)_{a \in \Ob(A)}.
  \]
  \begin{itemize}[label=-]
  \item The canonical map of $\ho(\oo\Cat^{\folk})$
    \[
    \hocolim_{a \in A}^{\folk} (A/a) \to A
    \]
    is an isomorphism thanks to Corollary \ref{cor:folkhmtpycol} applied to $\mathrm{id}_A : A \to A$.
  \item The canonical map of $\ho(\oo\Cat^{\Th})$
    \[
    \hocolim_{a \in A}^{\Th} (A/a) \to A
    \]
    is an isomorphism thanks to Corollary \ref{cor:thomhmtpycol} and the fact
    that the canonical morphisms of op-prederivators $\Ho(\Cat^{\Th}) \to
    \Ho(\oo\Cat^{\Th})$ is homotopy cocontinuous (see \ref{paragr:thomhmtpycol}).
    \item Every $A/a$ is \good{} thanks to Proposition \ref{prop:contractibleisgood} and Proposition \ref{prop:slicecontractible}.
  \end{itemize}
  Thus, Proposition \ref{prop:criteriongoodcat} applies and this proves that $A$ is \good{}.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
