\documentclass[12pt,a4paper,twoside,openright]{report}

\usepackage[dvipsnames]{xcolor}
\usepackage[unicode,psdextra,final]{hyperref}
\hypersetup{
  colorlinks=true,
  linkcolor=Purple,
  citecolor=cyan,  
}
%\usepackage{classicthesis}
\usepackage{mystyle}
\usepackage{times}
\usepackage{sectsty}
\allsectionsfont{\scshape}
\iffalse
%%% Watermark
\usepackage{draftwatermark}
\SetWatermarkText{DRAFT}
\SetWatermarkScale{2}
%%%
\fi
\usepackage{geometry}
\usepackage{fancyhdr}


%%% For line numbering (used for proodreading purposes)

%% \usepackage[pagewise,displaymath, mathlines]{lineno}
%% \linenumbers

\frenchspacing

\begin{document}
\newgeometry{margin=1in}
\begin{titlepage}
   \includegraphics[width=0.4\textwidth]{logo_UP.jpg}
    
       \vspace*{1cm}
   \raggedleft
   {
   \scshape{\textbf{Université de Paris}} \par}
 \vspace{0.25cm}
 {\large École de Sciences Mathématiques Paris Centre (ED 386)}
 
 {Institut de Recherche Fondamentale en Informatique (IRIF)}
 %{\large Institut de Recherche Fondamentale en Informatique (IRIF)}

 \vspace*{0.25cm}

 {\Large
   \scshape
   \textbf{Thèse de doctorat en Mathématiques}}

      {\large Dirigée par François Métayer \\
        et par Clemens Berger \par}

      \vspace*{2cm}
      \hrulefill
      \vspace*{0.25cm}
      
      \centering
        {\Huge \scshape \textbf{Homology of strict $\omega$-categories}\par}

        % \vspace{0.5cm}
        % \LARGE
        % Thesis Subtitle
        \vspace*{0.25cm}
        \hrulefill
        
        \vspace{1cm}

        {\Large
          
        \textbf{par Léonard Guetta}}

      



   \vspace*{2cm}
   {\slshape Présentée et soutenue publiquement en ligne le 28 janvier 2021 \\
     devant un jury composé de : \par}
    \vspace{0.8cm}

 
    \begin{tabular}{ll}
      \textbf{M. Benoît Fresse} (Prof., Université de Lille) & Rapporteur \\
      \textbf{Mme Eugenia Cheng} (Prof., School of the Art Institute) & Examinatrice \\
      \textbf{M. Carlos Simpson} (DR CNRS, Université de Nice) & Président du jury \\
      \textbf{M. Samuel Mimram} (Prof., École Polytechnique) & Examinateur \\
      \textbf{M. Georges Maltsiniotis} (DR CNRS, Université de Paris) & Examinateur \\
      \textbf{M. François Métayer} (MCF, Université de Paris Nanterre) & Directeur de Thèse \\
      \textbf{M. Clemens Berger} (MCF, Université de Nice) & Co--directeur de Thèse \\
      \textbf{M. Dimitri Ara} (MCF, Université Aix-Marseille) & Membre invité 
    \end{tabular}
    
      \vspace{0.8cm}
      {\slshape Rapporteur non présent à la soutenance : \par}
           \vspace{0.8cm}
    \begin{tabular}{ll}
      \textbf{M. Richard Garner} (Senior Lecturer, Macquarie University) & \\
    \end{tabular}
   %   \vspace{3cm}
      
      
     % \includegraphics[width=0.6\textwidth]{license.png}
              %  \vspace*{-3cm}
  
    \end{titlepage}

 
\selectlanguage{french}
\begin{abstract}
Dans cette thèse, on compare l'homologie \og classique \fg{} d'une
$\oo$\nbd{}catégorie (définie comme l'homologie de son nerf de Street) avec
son homologie polygraphique. Plus précisément, on prouve que les deux
homologies ne coïncident pas en général et qualifions d'\emph{homologiquement
  cohérentes} les $\oo$\nbd{}catégories particulières pour lesquelles l'homologie
polygraphique coïncide effectivement avec l'homologie du nerf. Le but poursuivi
est de trouver des critères abstraits et concrets permettant de détecter les
$\oo$\nbd{}catégories homologiquement cohérentes. Par exemple, on démontre que
toutes les (petites) catégories, que l'on considère comme des
$\oo$\nbd{}catégories strictes dont toutes les cellules au-delà de la dimension
$1$ sont des unités, sont homologiquement cohérentes. On introduit également la
notion de $2$\nbd{}catégorie \emph{sans bulles} et on conjecture qu'une
$2$\nbd{}catégorie cofibrante est homologiquement cohérente si et seulement si
elle est sans bulles. On démontre également des résultats importants concernant
les $\oo$\nbd{}catégories strictes qui sont libres sur un polygraphe, comme
le fait que si $F : C \to D$ est un $\oo$\nbd{}foncteur de Conduché discret et
si $D$ est libre sur un polygraphe alors $C$ l'est aussi. Dans son ensemble,
cette thèse établit un cadre général dans lequel étudier l'homologie des
$\oo$\nbd{}catégories en faisant appel à des outils d'algèbre homotopique
abstraite, tels que la théorie des catégories de modèles de Quillen ou la théorie
des dérivateurs de Grothendieck.

\bigskip

\noindent\textbf{Mots-clés : } Catégories supérieures, $\oo$\nbd{}catégories, homologie,
théorie de l'homotopie, polygraphes.
\end{abstract}
\selectlanguage{english}
\frenchspacing
\begin{abstract}
  In this dissertation, we compare the ``classical''
  homology of an $\oo$\nbd{}category (defined as the homology of its Street
  nerve) with its polygraphic homology. More precisely, we prove that both
  homologies generally do not coincide and call \emph{homologically coherent} the
  particular strict $\oo$\nbd{}categories for which polygraphic homology and
  homology of the nerve do coincide. The goal pursued is to find abstract
  and concrete criteria to detect homologically coherent $\oo$\nbd{}categories. For
  example, we prove that all (small) categories, considered as strict
  $\oo$\nbd{}categories with unit cells above dimension $1$, are homologically
  coherent. We also introduce the notion of \emph{bubble-free} $2$\nbd{}category
  and conjecture that a cofibrant $2$\nbd{}category is homologically
  coherent if and only if it is bubble-free.
  We also prove important results concerning free strict
  $\oo$\nbd{}categories on polygraphs (also known as computads), such as the
  fact that if $F : C \to D$ is a discrete Conduché $\oo$\nbd{}functor and $D$
  is a free strict $\oo$\nbd{}category on a polygraph, then so is $C$.
  Overall, this thesis achieves to build a general framework in which to study the
  homology of strict $\oo$\nbd{}categories using tools of abstract homotopical
  algebra such as Quillen's theory of model categories or Grothendieck's theory
  of derivators.
  
\bigskip

\noindent\textbf{Keywords : } Higher categories, $\oo$\nbd{}categories, homology,
homotopy theory, polygraphs.
  
\end{abstract}
\restoregeometry
\pagestyle{empty}
\clearpage\mbox{}\clearpage %% For a blank page

\pagenumbering{roman}
\pagestyle{plain}
\include{remerciements}
\tableofcontents

\newpage
\pagenumbering{arabic}
\pagestyle{fancy}
\fancyhf{}
\fancyfoot[C]{\thepage}
\fancyhead[RO,LE]{INTRODUCTION}
\include{introduction}
\fancyhf{}
\fancyfoot[C]{\thepage}
\fancyhead[RO,LE]{INTRODUCTION (FRANÇAIS)}
\include{introduction_fr}
\fancyhf{}
\fancyfoot[C]{\thepage}
\fancyhead[LE]{\leftmark}
\fancyhead[RO]{\rightmark}
\include{omegacat}
\include{homtheo}
\include{hmtpy}
\include{hmlgy}
\include{contractible}
\include{2cat}

\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{alpha}
\bibliography{memoire}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
