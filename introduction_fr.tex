\chapter*{Introduction (français)}
\addcontentsline{toc}{chapter}{Introduction (français)} %% For this chapter to
%% appear in toc
\selectlanguage{french}
Cette thèse a pour cadre général la \emph{théorie de l'homotopie des
$\oo$\nbd{}catégories strictes}, et, comme son titre le suggère, ce sont les
aspects homologiques de cette théorie qui sont traîtés. Le but est d'étudier et de
comparer deux invariants homologiques différents associés aux
$\oo$\nbd{}catégories strictes; c'est-à-dire, deux foncteurs différents
\[\mathbf{Str}\oo\Cat \to \ho(\Ch)\]
de la catégorie des $\oo$\nbd{}catégories strictes vers la catégorie homotopique
des complexes de chaînes en degré positif (i.e.\ la localisation de la catégorie
des complexes de chaînes en degré positif relativement aux
quasi\nbd{}isomorphismes).

Avant d'entrer dans le vif du sujet, précisons sans plus tarder qu'à
l'unique exception de la toute fin de cette introduction, toutes les
$\oo$\nbd{}catégories que nous considérerons sont des $\oo$\nbd{}catégories
\emph{strictes}. C'est pourquoi nous omettrons l'adjectif \og
strict\fg{} et parlerons simplement de \emph{$\oo$\nbd{}catégorie} plutôt
que de \emph{$\oo$\nbd{}catégorie stricte}. De même, nous noterons $\oo\Cat$ plutôt que
$\mathbf{Str}\oo\Cat$ la catégorie des $\oo$\nbd{}catégories (strictes).

\begin{named}[Le contexte : les $\oo$-categories en tant qu'espaces] L'étude de la théorie
  de l'homotopie des $\oo$\nbd{}catégories commence avec le foncteur nerf
  introduit par Street \cite{street1987algebra}
  \[
    N_{\omega} : \oo\Cat \to \Psh{\Delta}
  \]
  qui associe à toute $\oo$\nbd{}catégorie $C$ un ensemble simplicial
  $N_{\oo}(C)$, appelé le \emph{nerf de $C$}, généralisant le nerf usuel des
  (petites) catégories. En utilisant ce foncteur, il est possible de transférer
  la théorie de l'homotopie des ensembles simpliciaux aux $\oo$\nbd{}catégories,
  comme cela est fait par exemple dans les articles
  \cite{ara2014vers,ara2018theoreme,gagna2018strict,ara2019quillen,ara2020theoreme,ara2020comparaison}.
  Suivant la terminologie de ces derniers, un morphisme $f : C \to D$ de la catégorie
  $\oo\Cat$ est une \emph{équivalence de Thomason} si $N_{\oo}(f)$ est une
  équivalence faible de Kan--Quillen d'ensembles simpliciaux. Par définition, le
  foncteur nerf induit un foncteur au niveau des catégories homotopiques
    \[
    \overline{N_{\omega}} : \ho(\oo\Cat^{\Th}) \to \ho(\Psh{\Delta}),
  \]
  où $\ho(\oo\Cat^{\Th})$ est la localisation de $\oo\Cat$ relativement aux
  équivalences de Thomason et $\ho(\Psh{\Delta})$ est la localisation de
  $\Psh{\Delta}$ relativement aux équivalences faibles de Kan--Quillen d'ensembles
  simpliciaux. Comme l'a démontré Gagna \cite{gagna2018strict}, ce dernier foncteur est en
  fait une équivalence de catégories. Autrement dit, la théorie de l'homotopie
  des $\oo$\nbd{}catégories induite par les équivalences de Thomason est la
  même que la théorie de l'homotopie des espaces. Le résultat de Gagna est une
  généralisation du résultat analogue pour le nerf usuel des petites catégories,
  attribué à Quillen dans \cite{illusie1972complexe}. Dans le cas des petites
  catégories, Thomason a même démontré l'existence d'une structure de catégorie de modèles dont les équivalences
  faibles sont celles induites par le nerf \cite{thomason1980cat}. Le résultat analogue pour $\oo\Cat$ est toujours une
  conjecture \cite{ara2014vers}. 
\end{named}  

\begin{named}[Deux homologies pour les $\oo$-catégories]
  Armé du foncteur nerf de Street, il est naturel de définir le
  \emph{$k$\nbd{}ème groupe d'homologie d'une $\oo$\nbd{}catégorie $C$} comme
  étant le $k$\nbd{}ème groupe d'homologie du nerf de $C$. À la lumière du
  résultat de Gagna, ces groupes d'homologies sont simplement les groupes
  d'homologie des espaces vus sous un autre angle. Afin d'éviter de potentielles
  confusions à venir, nous appellerons désormais ces groupes d'homologie les
  \emph{groupes d'homologie singulière} de $C$ et nous utiliserons la notation
  $H^{\sing}_k(C)$.

  D'autres groupes d'homologie pour les $\oo$\nbd{}catégories ont aussi été
  définis par Métayer dans \cite{metayer2003resolutions}. La définition de
  ceux-ci repose sur la notion de \emph{$\oo$\nbd{}catégorie libre sur un
    polygraphe} (aussi connue sous le nom de \emph{computade}), c'est-à-dire
  de $\oo$\nbd{}catégorie obtenue de manière récursive à partir de la catégorie
  vide en attachant librement des cellules. Désormais, nous dirons simplement
  \emph{$\oo$\nbd{}catégorie libre}. Il a été observé par Métayer que toute
  $\oo$\nbd{}catégorie $C$ admet une \emph{résolution polygraphique},
  c'est-à-dire qu'il existe une $\oo$\nbd{}catégorie libre $P$ et un morphisme
  de $\oo\Cat$
  \[
    f : P \to C,
  \]
  satisfaisant des propriétés analogues à celles des fibrations triviales
  d'espaces topologiques (ou d'ensemples simpliciaux). De plus, toute
  $\oo$\nbd{}catégorie libre $P$ peut être \og abélianisée \fg{} en un complexe
  de chaînes $\lambda(P)$ et il a été démontré par Métayer que pour deux
  résolutions polygraphiques $P \to C$ et $P' \to C$ d'une même
  $\oo$\nbd{}catégorie, les complexes de chaînes $\lambda(P)$ et $\lambda(P')$
  sont quasi-isomorphes. Ainsi, on peut définir le \emph{$k$\nbd{}ème groupe
    d'homologie polygraphique} de $C$, noté $H_k^{\pol}(C)$, comme étant le
  $k$\nbd{}ème groupe d'homologie de $\lambda(P)$ pour n'importe quelle
  résolution polygraphique $P \to C$.

  Ces considérations invitent à se poser la question suivante :
  \begin{center}
    A-t-on $H_{\bullet}^{\pol}(C) \simeq H_{\bullet}^{\sing}(C)$ pour toute
    $\oo$\nbd{}catégorie $C$ ?
  \end{center}
  Une première réponse partielle a été donnée par Lafont et
  Métayer \cite{lafont2009polygraphic} : pour tout monoïde $M$ (vu comme une $\oo$\nbd{}catégorie
  à un seul objet et dont toutes les cellules de dimension supérieure à $1$ sont
  des unités), on a $H_{\bullet}^{\pol}(M) \simeq H_{\bullet}^{\sing}(M)$.
  Mentionnons au passage que l'homologie polygraphique a été conçue originellement pour étudier l'homologie des monoïdes et fait partie d'un programme
  dont le but est de généraliser en dimension supérieure les travaux de Squier
  sur la théorie de la réécriture des monoïdes \cite{guiraud2006termination,
    lafont2007algebra, guiraud2009higher, guiraud2018polygraphs}. Malgré le
  résultat de Lafont et Métayer, la réponse générale à la
  question précédente est \emph{non}. Un contre-exemple a été découvert par
  Maltsiniotis et Ara. Soit $B$ le monoïde commutatif $(\mathbb{N},+)$, vu
  comme une $2$\nbd{}catégorie avec une seule $0$\nbd{}cellule et pas de
  $1$\nbd{}cellule non-triviale. Cette $2$\nbd{}catégorie est libre (en tant
  que $\oo$\nbd{}catégorie) et un calcul rapide montre que:
  \[
    H_k^{\pol}(B)=\begin{cases} \mathbb{Z} &\text{ pour } k=0,2 \\ 0 &\text{
        sinon. }\end{cases}
  \]
  D'autre part, Ara a démontré \cite[Theorem 4.9 et Example
  4.10]{ara2019quillen} que le nerf de $B$ est un $K(\mathbb{Z},2)$, qui a donc
  des groupes d'homologie non triviaux en toute dimension paire.

  La question devient ainsi :
  \begin{center}
    \textbf{(Q)} Quelles sont les $\oo$\nbd{}catégories $C$ pour lesquelles
    $H_{\bullet}^{\pol}(C) \simeq H_{\bullet}^{\sing}(C)$?
  \end{center}
  C'est précisément à cette question que tente de répondre cette thèse.
  Néanmoins, le lecteur trouvera dans ce document plusieurs notions nouvelles et
  résultats qui, bien qu'originellement motivés par la question ci-dessus, sont
  intrinsèquement intéressants pour la théorie des $\oo$\nbd{}catégories et
  dont la portée dépasse les considérations homologiques précédentes.
\end{named}
\begin{named}[Une autre formulation du problème] Un des accomplissements
  du travail présenté ici est l'établissement d'un cadre conceptuel qui permet
  une reformulation plus abstraite et plus satisfaisante de la question
  de comparaison de l'homologie polygraphique et de l'homologie singulière des
  $\oo$\nbd{}catégories.

  Pour cela, rappelons tout d'abord qu'une variation de l'équivalence de
  Dold--Kan (voir par exemple \cite{bourn1990another}) permet d'affirmer que la
  catégorie des objets en groupes abéliens dans la catégorie $\oo\Cat$ est
  équivalente à la catégorie des complexes de chaînes en degré positif
  \[
    \Ab(\oo\Cat) \simeq \Ch.
  \]
  Ainsi, on a un foncteur d'oubli $\Ch \simeq \Ab(\oo\Cat) \to \oo\Cat$, qui
  admet un adjoint à gauche
  \[
    \lambda : \oo\Cat \to \Ch.
  \]
  En outre, pour une $\oo$\nbd{}catégorie \emph{libre} $C$, le complexe de
  chaînes $\lambda(C)$ est exactement celui obtenu par le processus
  d'\og abélianisation \fg{}
  que Métayer utilise dans sa définition d'homologie polygraphique.
  
  Par ailleurs, la catégorie $\oo\Cat$ peut être munie d'une structure de
  catégorie de modèles, communément appelée \emph{la structure de catégorie de modèles folk}
  \cite{lafont2010folk}, dont les équivalences faibles sont les
  \emph{équivalences de $\oo$\nbd{}catégories} (notion généralisant celle d'équivalence de
  catégories) et dont les objets cofibrants sont les $\oo$\nbd{}catégories
  libres \cite{metayer2008cofibrant}. Les résolutions polygraphiques ne sont alors
  rien d'autre que des remplacements cofibrants pour cette structure de catégorie de modèles.
  Comme la définition des groupes d'homologie polygraphique le laissait deviner,
  le foncteur $\lambda$ est Quillen à gauche relativement à cette structure de
  catégorie de modèles. En particulier, ce foncteur admet un foncteur dérivé à gauche
  \[
    \LL \lambda^{\folk} : \ho(\oo\Cat^{\folk}) \to \ho(\Ch)
  \]
  et on a tautologiquement $H_k^{\pol}(C) = H_k(\LL \lambda^{\folk}(C))$ pour
  toute $\oo$\nbd{}catégorie $C$ et pour tout $k\geq 0$. Désormais, on posera même
  \[
    \sH^{\pol}(C):=\LL \lambda^{\folk}(C).
  \]
  Je précise que cette façon de comprendre l'homologie polygraphique comme foncteur dérivé à
  gauche fait partie du folklore depuis un certain temps et je ne prétends à
  aucune originalité concernant ce point précis.

  D'autre part, le foncteur $\lambda$ est aussi dérivable à gauche quand
  $\oo\Cat$ est munie des équivalences de Thomason, ce qui permet d'obtenir un
  foncteur dérivé à gauche
    \[
    \LL \lambda^{\Th} : \ho(\oo\Cat^{\Th}) \to \ho(\Ch).
  \]
  En outre, ce foncteur est tel que  $ H_k^{\sing}(C) = H_k(\LL
  \lambda^{\Th}(C))$ pour toute $\oo$\nbd{}catégorie $C$ et pour tout $k \geq
  0$. Contrairement au cas \og folk \fg{}, ce résultat est
  complètement nouveau et apparaît pour la première fois dans ce manuscrit (à ma
  connaissance, du moins). Notons également que, puisque l'existence d'une
  structure de catégorie de modèles \og à la Thomason \fg{} sur $\oo\Cat$
  est toujours conjecturale, les outils habituels de la théorie de Quillen des
  catégories de modèles sont inutilisables pour démontrer que $\lambda$ est
  dérivable à gauche. La difficulté fut de trouver un moyen de
  contourner ce problème.

  Désormais, on posera
  \[
    \sH^{\sing}(C):=\LL \lambda^{\Th}(C).
  \]

  Finalement, on peut montrer que toute équivalence de $\oo$\nbd{}catégories
  est une équivalence de Thomason. Ainsi, le foncteur identité de $\oo\Cat$
  induit formellement un foncteur $\J$ au niveau des catégories homotopiques
  \[
    \J : \ho(\oo\Cat^{\folk}) \to \ho(\oo\Cat^{\Th}),
  \]
  et on obtient donc un triangle
  \[
    \begin{tikzcd}
      \ho(\oo\Cat^{\folk}) \ar[rd,"\LL \lambda^{\folk}"'] \ar[r,"\J"] & \ho(\oo\Cat^{\Th}) \ar[d,"\LL \lambda^{\Th}"] \\
      & \ho(\Ch).
    \end{tikzcd}
  \]
  Ce triangle n'est \emph{pas} commutatif (même à un isomorphisme près), car
  cela impliquerait que les groupes d'homologie singulière et les groupes
  d'homologie polygraphique coïncident pour toute $\oo$\nbd{}catégorie.
  Néanmoins, puisque les foncteurs $\LL \lambda^{\folk}$ et $\LL \lambda^{\Th}$
  sont tous les deux des foncteurs dérivés à gauche du même foncteur, l'existence
  d'une transformation naturelle $\pi : \LL \lambda^{\Th} \circ \J
  \Rightarrow \LL \lambda^{\folk}$ découle formellement par propriété
  universelle. De plus, $\J$ étant l'identité sur les objets, cette
  transformation naturelle fournit pour toute $\oo$\nbd{}catégorie $C$ un
  morphisme
    \[
    \pi_C : \sH^{\sing}(C) \to \sH^{\pol}(C),
  \]
  que nous appellerons le \emph{morphisme de comparaison canonique}. Une $\oo$\nbd{}catégorie $C$ est qualifiée d'\emph{homologiquement cohérente} si $\pi_C$ est
  un isomorphisme (ce qui signifie exactement que le
  morphisme induit $H^{\sing}_k(C) \to
  H_k^{\pol}(C)$ est un isomorphisme pour tout $k \geq 0$). La question devient alors : 
  \begin{center}
    \textbf{(Q')} Quelles $\oo$\nbd{}catégories sont homologiquement cohérentes ?
  \end{center}
  Notons au passage que la question \textbf{(Q')} est théoriquement plus précise
  que la question \textbf{(Q)}. Cependant, dans tous les exemples concrets
  que nous rencontrerons, c'est toujours à la question \textbf{(Q')} que nous répondrons.

  Comme il sera expliqué dans la thèse, de cette
  reformulation en terme de foncteurs dérivés, il est également possible de déduire formellement  que l'homologie polygraphique n'est \emph{pas} invariante
  relativement aux équivalences de Thomason. Cela signifie qu'il existe au moins
  une équivalence de Thomason $f : C \to D$ telle que le morphisme induit en
  homologie polygraphique
  \[
    \sH^{\pol}(C) \to \sH^{\pol}(D)
  \]
  n'est \emph{pas} un isomorphisme. En d'autres termes, si nous voyons les
  $\oo$\nbd{}catégories comme modèles des types d'homotopie (via la
  localisation de $\oo\Cat$ relativement aux équivalences de Thomason), alors
  l'homologie polygraphique n'est \emph{pas} un invariant bien défini. Un autre
  point de vue possible serait de considérer que l'homologie polygraphique est
  un invariant intrinsèque des $\oo$\nbd{}catégories (et non pas à équivalence
  de Thomason près) et, de cette façon, est un invariant plus fin que
  l'homologie singulière. Ce n'est pas le point de vue adopté dans cette thèse
  et ce choix sera motivé à la fin de cette introduction. Le
  slogan à retenir est :
  \begin{center}
    L'homologie polygraphique est un moyen de calculer les groupes d'homologie
    singulière des $\oo$\nbd{}catégories homologiquement cohérentes.
  \end{center}
  L'idée étant que pour une $\oo$\nbd{}catégorie libre $P$ (qui est donc sa propre résolution polygraphique), le complexe de chaînes
  $\lambda(P)$ est beaucoup moins \og gros \fg{} que le
  complexe de chaînes associé au nerf de $P$, et ainsi les groupes d'homologie
  polygraphique de $P$ sont beaucoup plus faciles à calculer que les groupes
  d'homologie singulière. La situation est comparable à l'utilisation de
  l'homologie cellulaire afin de calculer les groupes d'homologie singulière
  d'un CW\nbd{}complexe. La différence étant que dans ce dernier cas il est toujours
  possible de procéder ainsi, alors que dans le cas des $\oo$\nbd{}catégories,
  on doit d'abord s'assurer que la $\oo$\nbd{}catégorie (libre) en question est
  bien homologiquement cohérente.
\end{named}
\begin{named}[Détecter les $\oo$-catégories qui sont homologiquement cohérentes]
  Un des principaux résultats de cette thèse est le suivant :
  \begin{center}
    Toute (petite) catégorie est homologiquement cohérente.
  \end{center}
  Afin de donner du sens à cette assertion, il faut considérer les catégories
  comme des $\oo$\nbd{}catégories dont les cellules au delà de la dimension $1$
  sont des unités. Le résultat ci-dessus n'est pas pour autant trivial car pour
  une résolution polygraphique $P \to C$ d'une petite catégorie $C$, la
  $\oo$\nbd{}catégorie $P$, elle, n'a pas forcément que des cellules unités au
  delà de la dimension 1.

  En tant que tel, ce résultat est seulement une petite généralisation du
  résultat de Lafont et Métayer sur les monoïdes (bien qu'il soit plus précis,
  même restreint aux monoïdes, car il dit que c'est le \emph{morphisme de
    comparaison canonique} qui est un isomorphisme). Mais la véritable nouveauté
  du résultat en est sa démonstration qui est plus conceptuelle que celle de
  Lafont et Métayer. Elle repose sur l'introduction de nouvelles notions et le
  développement de nouveaux résultats; le tout s'assemblant élégamment pour
  finalement produire le résultat voulu. Cette thèse a été écrite de telle façon
  que tous les élements nécessaires à la démonstration du résultat précédent
  sont répartis sur plusieurs chapitres; une version plus condensée de celle-ci
  se trouve dans l'article \cite{guetta2020homology}. Parmi les nouvelles notions
  développées, la plus significative est probablement celle
  de $\oo$\nbd{}foncteur de Conduché discret. Un $\oo$\nbd{}foncteur $f : C \to
  D$ est un \emph{$\oo$\nbd{}foncteur de Conduché discret} quand pour toute
  cellule $x$ de $C$, si $f(x)$ peut être décomposé en
  \[
    f(x)=y'\comp_k y'',
  \]
  alors il existe une unique paire $(x',x'')$ de cellules de $C$ qui sont
  $k$\nbd{}composables et telles que
  \[
    f(x')=y',\, f(x'')=y'' \text{ and } x=x'\comp_k x''.
  \]
  Le résultat principal démontré concernant cette notion est que pour tout
  $\oo$\nbd{}foncteur de Conduché discret $f : C \to D$, si $D$ est libre alors
  $C$ est aussi libre. La démonstration est longue et fastidieuse, bien que
  relativement facile d'un point de vue conceptuel, et apparaît pour la première
  fois dans le papier \cite{guetta2020polygraphs}, qui lui est dédié.

  Une fois le cas de l'homologie des ($1$\nbd{})catégories complètement résolu,
  il est naturel de s'intéresser aux $2$\nbd{}catégories. Contrairement au cas
  des (1\nbd{})catégories, les 2\nbd{}catégories ne sont pas toutes
  homologiquement cohérentes et la situation est beaucoup plus compliquée. En
  premier lieu, on peut se restreindre aux $2$\nbd{}catégories qui sont libres
  (en tant que $\oo$\nbd{}catégories) et c'est ce qui est fait dans cette thèse.
  Avec cette hypothèse simplificatrice, le problème de caractérisation des
  $2$\nbd{}catégories libres homologiquement cohérentes peut être réduit à la
  question suivante : soit un carré cocartésien de la forme
  \[
    \begin{tikzcd}
      \sS_1 \ar[r] \ar[d] & P \ar[d]\\
      \sD_2 \ar[r] & P', \ar[from=1-1,to=2-2,"\ulcorner",phantom,very near end]
    \end{tikzcd}
  \]
  où $P$ est une $2$\nbd{}catégorie libre, quand est-il \emph{homotopiquement
    cocartésien} relativement aux équivalences de Thomason? En conséquence, une
  partie substantielle du travail présenté ici consiste à développer des outils
  permettant de reconnaître les carrés homotopiquement cocartésiens de
  $2$\nbd{}catégories relativement aux équivalences de Thomason. Bien que les
  outils qui seront présentés ne permettent pas de répondre entièrement à la
  question ci-dessus, ils permettent tout de même de détecter de tels carrés
  cocartésiens dans beaucoup de situations concrètes. Il y a même une section
  entière de la thèse qui consiste uniquement en une liste d'exemples détaillés
  de calculs du type d'homotopie de $2$\nbd{}catégories libres en utilisant ces
  outils. De ces exemples se dégage très clairement une classe particulière de
  $2$\nbd{}catégories, que j'ai nommées les \og $2$\nbd{}catégories sans bulles
  \fg{} et qui sont caractérisées comme suit. Pour une $2$\nbd{}catégorie,
  appelons \emph{bulle} une $2$\nbd{}cellule non-triviale dont la source et le
  but sont des unités sur une $0$\nbd{}cellule (nécessairement la même). Une
  \emph{$2$\nbd{}catégorie sans bulles} est simplement une $2$\nbd{}catégorie
  qui n'a aucune bulle. L'archétype de la $2$\nbd{}catégorie qui n'est
  \emph{pas} sans bulles est la $2$\nbd{}catégorie $B$ que nous avons déjà
  rencontrée plus haut (c'est-à-dire le monoïde commutatif $(\mathbb{N},+)$ vu
  comme une $2$\nbd{}catégorie). Comme dit précédemment, cette
  $2$\nbd{}catégorie n'est pas homologiquement cohérente et cela ne semble pas
  être une coïncidence. Il est tout à fait remarquable que de toutes les $2$\nbd{}catégories étudiées dans cette thèse, les seules qui ne
  sont pas homologiquement cohérentes sont exactement celles qui ne sont
  \emph{pas} sans bulles. Cela conduit à la conjecture ci-dessous, qui est le
  point d'orgue de la thèse.
  \begin{center}
    \textbf{(Conjecture)} Une $2$\nbd{}catégorie libre est homologie cohérente
    si et seulement si elle est sans bulles.
  \end{center}
\end{named}
  \begin{named}[Une vue d'ensemble]
    Terminons cette introduction par un autre point
    de vue sur la comparaison des homologies polygraphique et singulière.
    Précisons immédiatement que ce point de vue est hautement conjectural et
    n'est pas du tout abordé dans le reste de la thèse. Il s'agit plus d'un
    guide pour des travaux futurs qu'autre chose.

    De la même façon que les $2$\nbd{}catégories (strictes) sont des cas
    particuliers de bicatégories, les $\oo$\nbd{}catégories strictes sont en
    réalité des cas particuliers de ce qui est communément appelé des
    \emph{$\oo$\nbd{}catégories faibles}. Ces objets mathématiques ont été
    définis, par exemple, par Batanin en utilisant le formalisme des opérades
    globulaires \cite{batanin1998monoidal} ou par Maltsiniotis en suivant des
    idées de Grothendieck \cite{maltsiniotis2010grothendieck}. Tout comme la
    théorie des quasi-catégories (qui est un modèle homotopique pour la théorie
    des $\oo$\nbd{}catégories faibles dont les cellules sont toutes inversibles
    au delà de la dimension 1) s'exprime avec le même langage que la théorie des
    catégories usuelle, il est attendu que toutes les notions \og
    intrinsèques \fg{} (dans un sens précis à définir) de la théorie des
    $\oo$\nbd{}catégories strictes ont des analogues \emph{faibles}. Par
    exemple, il est attendu qu'il y ait une structure de catégorie de modèles folk sur la
    catégorie des $\oo$\nbd{}catégories faibles et qu'il y ait une bonne notion
    de $\oo$\nbd{}catégorie faible libre. En fait, ces dernières seraient
    certainement définies comme les $\oo$\nbd{}catégories faibles qui sont
    récursivement obtenues à partir de la $\oo$\nbd{}catégorie vide en attachant librement
    des cellules, ce qui est l'analogue formelle du cas strict. Le point clé ici
    est qu'une $\oo$\nbd{}catégorie stricte libre n'est \emph{jamais} libre en
    tant que $\oo$\nbd{}catégorie faible (excepté la $\oo$\nbd{}catégorie vide).
    Par ailleurs, il existe de bons candidats pour l'homologie polygraphique des
    $\oo$\nbd{}catégories faibles qui sont obtenus par mimétisme de la
    définition du cas strict. Mais il n'y aucune raison en général que
    l'homologie polygraphique d'une $\oo$\nbd{}catégorie stricte soit la même
    que son \og homologie polygraphique faible \fg{}. En effet, puisque les
    $\oo$\nbd{}catégories strictes libres ne sont pas libres en tant
    que $\oo$\nbd{}catégories faibles, prendre une \og résolution polygraphique
    faible \fg{} d'une $\oo$\nbd{}catégorie libre ne revient pas à
    prendre une résolution polygraphique. De fait, lorsqu'on essaye de calculer
    l'homologie polygraphique faible de $B$, il semblerait que cela donne les groupes
    d'homologie d'un $K(\mathbb{Z},2)$, ce qui aurait été attendu de l'homologie
    polygraphique au départ. De cette observation, il est tentant de faire la
    conjecture suivante :
    \begin{center}
      L'homologie polygraphique faible d'une $\oo$\nbd{}categorie stricte
        coïncide avec son homologie singulière. 
    \end{center}
    En d'autres termes, nous conjecturons que le fait que l'homologie
    polygraphique et l'homologie singulière d'une $\oo$\nbd{}catégorie stricte
    ne coïncident pas est un défaut dû à un cadre de travail trop étroit. La \og
    bonne \fg{} définition de l'homologie polygraphique devrait être la faible.

    Nous pourrions même aller plus loin et conjecturer la même chose pour les
    $\oo$\nbd{}catégories faibles. Pour cela, il est nécessaire de disposer
    d'une définition de l'homologie singulière des $\oo$\nbd{}catégories
    faibles. Conjecturellement, on procède de la manière suivante. À toute
    $\oo$\nbd{}catégorie faible $C$, on peut associer un $\oo$\nbd{}groupoïde
    faible $L(C)$ en inversant formellement toutes les cellules de $C$. Puis, si
    on en croit la conjecture de Grothendieck (voir
    \cite{grothendieck1983pursuing} et \cite[Section
    2]{maltsiniotis2010grothendieck}), la catégorie des $\oo$\nbd{}groupoïdes
    faibles munie des équivalences de $\oo$\nbd{}groupoïdes faibles (voir
    \cite[Paragraph 2.2]{maltsiniotis2010grothendieck}) est un modèle de la
    théorie homotopique des espaces. En particulier, chaque $\oo$\nbd{}groupoïde
    a des groupes d'homologie et on peut définir les groupes d'homologie
    singulière d'une $\oo$\nbd{}catégorie faible $C$ comme les groupes
    d'homologie de $L(C)$.
  \end{named}
  \begin{named}[Organisation de la thèse]
    Dans le premier chapitre, nous passerons en revue quelques aspects de la
    théorie des $\oo$\nbd{}catégories. En particulier, nous étudierons avec
    grand soin les $\oo$\nbd{}catégories libres, qui sont au c\oe{}ur de cette
    thèse. C'est le seul chapitre de la thèse qui ne contient aucune référence à
    la théorie de l'homotopie. C'est également dans ce chapitre que nous introduirons la notion
    de $\oo$\nbd{}foncteur de Conduché discret et que nous étudierons leur
    relation avec les $\oo$\nbd{}catégories libres. Le point
    culminant du chapitre étant le théorème \ref{thm:conduche}, qui dit que pour
    un $\oo$\nbd{}foncteur de Conduché discret $F : C \to D$, si $D$ est libre,
    alors $C$ l'est aussi. La démonstration de ce théorème est longue et
    technique et est décomposée en plusieurs parties distinctes.

    Le second chapitre a pour but de rappeler quelques outils d'algèbre
    homotopique. En particulier, les aspects élémentaires de la
    théorie des colimites homotopiques en utilisant le formalisme de Grothendieck des
    dérivateurs y sont rapidement présentés. Notons au passage que ce chapitre
    ne contient \emph{aucun} résultat original et peut être omis en première
    lecture. Son unique raison d'être est de donner au lecteur un catalogue de
    résultats concernant les colimites homotopiques qui seront utilisés par la
    suite.

    Dans le troisième chapitre, nous aborderons enfin la théorie de l'homotopie
    des $\oo$\nbd{}catégories. C'est là que seront définies et comparées entre elles les différentes
    notions d'équivalences faibles pour les $\oo$\nbd{}catégories. Les deux
    résultats les plus significatifs de ce chapitre sont probablement
    la proposition \ref{prop:folkisthom}, qui dit que toute équivalence de
    $\oo$\nbd{}catégorie est une équivalence de Thomason, et le théorème
    \ref{thm:folkthmA}, qui dit que les équivalences de $\oo$\nbd{}catégories
    satisfont une propriété réminiscente du théorème A de Quillen \cite[Theorem
    A]{quillen1973higher} et sa généralisation $\oo$\nbd{}catégorique par Ara
    et Maltsiniotis \cite{ara2018theoreme,ara2020theoreme}.

    Dans le quatrième chapitre, nous définirons les homologies polygraphique et
    singulière des $\oo$\nbd{}catégories et fomulerons précisément le problème
    de leur comparaison. Jusqu'à la section \ref{section:polygraphichmlgy}
    incluse, tous les résultats étaient connus avant cette thèse (au moins dans
    le folklore), mais à partir de la section \ref{section:singhmlgyderived}
    tous les résultats sont orignaux. Trois résultats fondamentaux de ce
    chapitre sont les suivants : le théorème \ref{thm:hmlgyderived}, qui dit que
    l'homologie singulière s'obtient comme le foncteur dérivé d'un foncteur
    d'abélianisation, la proposition \ref{prop:criteriongoodcat}, qui donne un
    critère abstrait pour détecter les $\oo$\nbd{}catégories homologiquement
    cohérentes, et la proposition \ref{prop:comphmlgylowdimension}, qui dit que les
    groupes d'homologies polygraphique et singulière coïncident toujours en basse
    dimension.

    Le cinquième chapitre a pour but de démontrer le théorème fondamental
    \ref{thm:categoriesaregood}, qui dit que toute catégorie est homologiquement
    cohérente. Pour cela, nous nous intéresserons en premier lieu
    à une classe particulière de $\oo$\nbd{}catégories, dites
    \emph{contractiles}, et nous montrerons que toute $\oo$\nbd{}catégorie
    contractile est homologiquement cohérente (Proposition
    \ref{prop:contractibleisgood}).

    Enfin, le sixième et dernier chapitre de la thèse s'intéresse à l'homologie
    des $2$\nbd{}catégories libres. Le but est d'essayer de comprendre quelles
    sont les $2$\nbd{}catégories libres qui sont homologiquement cohérentes.
    Pour cela, un critère pour détecter les carrés homotopiquement cocartésiens
    relativement aux équivalences de Thomason y est donné (Proposition
    \ref{prop:critverthorThomhmtpysquare}). Ce critère est fondé sur la théorie
    de l'homotopie des ensembles bisimpliciaux. Ensuite, nous appliquerons ce
    critère ainsi que d'autres techniques \emph{ad hoc} au calcul du type d'homotopie d'un grand
    nombre de $2$\nbd{}catégories libres. La conclusion du chapitre est la
    conjecture \ref{conjecture:bubblefree}, qui énonce qu'une $2$\nbd{}catégorie
    libre est homologiquement cohérente si et seulement si elle est sans bulles.
    \end{named}
\selectlanguage{english}
\frenchspacing
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
